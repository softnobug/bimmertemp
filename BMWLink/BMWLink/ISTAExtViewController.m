//
//  ISTAExtViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "ISTAExtViewController.h"

@interface ISTAExtViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lefttitleLa;
@property (weak, nonatomic) IBOutlet UILabel *leftsubTitleLa;
@property (weak, nonatomic) IBOutlet UILabel *vimcarLa;

@end

@implementation ISTAExtViewController
static NSArray *mcontentArr;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLa.text = self.contentArr[0];
    self.contentLa.text = @"";
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.vimcarLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    self.lefttitleLa.text = @"ISTA+";
    self.leftsubTitleLa.text = @"硬件诊断";
    mcontentArr = self.contentArr;
    NSLog(@"contentArr:%@",self.contentArr);
    
}

+(void)receIve:(NSString*)recStr{
    
    NSString *s = mcontentArr[1];
    s = [NSString stringWithFormat:@"6%@",s];
    
    if ([recStr hasPrefix:s]) {
        NSInteger desm = [mcontentArr[4] integerValue];
        NSString *desStr = [recStr substringWithRange:NSMakeRange(7+(desm-1)*2, 2)];
        NSLog(@"目标值:%@",desStr);
        
    }
    
    
    
}



- (IBAction)startAction:(id)sender {
    
//    [self sendMBData:self.contentArr[1]];
    
}

- (IBAction)endAction:(id)sender {
    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"istaEXtStop" object:nil userInfo:nil];
    
}


-(void)sendMBData:(NSString *)message{
    
    NSArray *arr = [linkManager readFile:@"ISTA_top.txt"];
    NSMutableArray *mArr = @[].mutableCopy;
    
    for (NSString *ss in arr) {
        
        NSString *tss = [ss stringByReplacingOccurrencesOfString:@"XX" withString:message];
        
        [mArr addObject:tss];
        
    }
    
    [mArr addObject:self.contentArr[2]];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"istaEXt" object:nil userInfo:@{@"ista":mArr}];
    
    NSLog(@"%@",mArr);
    
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
