//
//  numManager.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/21.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface numManager : NSObject

//16进制转10进制
+(NSString *)hexToTen:(NSString *)getS;

@end
