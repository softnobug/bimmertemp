//
//  socketManager.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//


#import "socketManager.h"
#import <SystemConfiguration/CaptiveNetwork.h>


@implementation socketManager
static GCDAsyncSocket*clientSocket;
static resultBlock conSuc;
static resultBlock conErr;
static resultBlock conFail;
static receiveData recDataBlo;


+ (NSString *)getWifiName

{
    
    NSString *wifiName = nil;
    
    
    
    CFArrayRef wifiInterfaces = CNCopySupportedInterfaces();
    
    
    
    if (!wifiInterfaces) {
        
        return nil;
        
    }
    
    
    
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    
    
    
    for (NSString *interfaceName in interfaces) {
        
        CFDictionaryRef dictRef = CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        
        
        
        if (dictRef) {
            
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            
//            NSLog(@"network info -> %@", networkInfo);
            
            wifiName = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            
            
            
            CFRelease(dictRef);
            
        }
        
    }
    
    
    
    CFRelease(wifiInterfaces);
    
    return wifiName;
    
}

+(NSString *)localWiFiIPAddress
{
    BOOL success;
    struct ifaddrs * addrs;
    const struct ifaddrs * cursor;
    
    success = getifaddrs(&addrs) == 0;
    if (success) {
        cursor = addrs;
        while (cursor != NULL) {
            // the second test keeps from picking up the loopback address
            if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0)
            {
                NSString *name = [NSString stringWithUTF8String:cursor->ifa_name];
                if ([name isEqualToString:@"en0"])  // Wi-Fi adapter
                    return [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr)];
            }
            cursor = cursor->ifa_next;
        }
        freeifaddrs(addrs);
    }
    return nil;
}



+(void)initSocketConDid:(resultBlock)conSuccess conError:(resultBlock)conError conFail:(resultBlock)confail {
    
    conSuc = conSuccess;
    conErr = conError;
    conFail = confail;
    
    socketManager *so = [[socketManager alloc]init];
    
    clientSocket = [[GCDAsyncSocket alloc]initWithDelegate:so delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    [clientSocket connectToHost:@"192.168.0.10" onPort:35000 viaInterface:nil withTimeout:-1 error:&error];
    
}

//发送数据
+(void)sendData:(NSString*)message{
    
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    // withTimeout -1 : 无穷大,一直等
    // tag : 消息标记
    [clientSocket writeData:data withTimeout:- 1 tag:0];
    
}

//读取服务端数据
+ (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    
    NSString *text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"读取服务端数据:%@",text);
    
    // 读取到服务端数据值后,能再次读取
    [clientSocket readDataWithTimeout:- 1 tag:0];
    
}

//连接成功
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    
    NSLog(@"服务器IP:%@,端口:%d",host,port);
    // 连接成功开启定时器
    //    [self addTimer];
    // 连接后,可读取服务端的数据
//    [self.clientSocket readDataWithTimeout:- 1 tag:0];
//    self.connected = YES;
        
}

//断开连接
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    
    NSLog(@"断开连接");
    clientSocket.delegate = nil;
    clientSocket = nil;
//    self.connected = NO;
//    [self.connectTimer invalidate];
    
    
}

//// 添加定时器
//- (void)addTimer
//{
//    // 长连接定时器
//    self.connectTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(longConnectToSocket) userInfo:nil repeats:YES];
//    // 把定时器添加到当前运行循环,并且调为通用模式
//    [[NSRunLoop currentRunLoop] addTimer:self.connectTimer forMode:NSRunLoopCommonModes];
//
//}
//
//// 心跳连接
//- (void)longConnectToSocket
//{
//    // 发送固定格式的数据,指令@"longConnect"
//    float version = [[UIDevice currentDevice] systemVersion].floatValue;
//    NSString *longConnect = [NSString stringWithFormat:@"123%f",version];
//
//    NSData  *data = [longConnect dataUsingEncoding:NSUTF8StringEncoding];
//
//    [self.clientSocket writeData:data withTimeout:- 1 tag:0];
//
//}


@end
