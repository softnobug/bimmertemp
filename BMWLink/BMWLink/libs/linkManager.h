//
//  linkManager.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/17.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface linkManager : NSObject
+(NSString *)stringToAscII:(NSString *)str;
+ (NSString *)stringFromHexString:(NSString *)hexString;
+ (NSArray *)readFile:(NSString *)fileName;
+ (NSDictionary*)readTxtFile:(NSString*)fileName;
+(NSArray *)tValueToArr:(NSString *)fileName;
+(NSArray*)arrContainArr:(NSString *)fileName;
@end
