//
//  UIView+Frame.h
//  Frame
//
//  Created by Yubin on 2017/7/28.
//  Copyright © 2017年 X. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property (nonatomic,assign) CGFloat x;
@property (nonatomic,assign) CGFloat y;
@property (nonatomic,assign) CGFloat width;
@property (nonatomic,assign) CGFloat height;

@end
