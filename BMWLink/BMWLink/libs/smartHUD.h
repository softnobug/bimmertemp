//
//  smartHUD.h
//  wheelservice
//
//  Created by 孟维学 on 2017/1/8.
//  Copyright © 2017年 wheelheres. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface smartHUD : NSObject
//{
//    MBProgressHUD *textHUD;
//}
//@property(nonatomic,strong)MBProgressHUD *textHUD;
+(void)alertText:(NSString *)message view:(UIView *)addview delay:(NSInteger)delaytime after:(void(^)(void))OK;
+(void)alertText:(UIView *)view alert:(NSString *)alert delay:(NSInteger)delaytime;
+(void)progressShow:(UIView *)view alertText:(NSString *)alert;
+(void)Hide;
@end
