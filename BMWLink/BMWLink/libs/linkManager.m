//
//  linkManager.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/17.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "linkManager.h"

@implementation linkManager


+ (NSString *)stringFromHexString:(NSString *)hexString { //
    
    char *myBuffer = (char *)malloc((int)[hexString length] / 2 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
    }
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    return unicodeString;
    
}

#pragma mark string 转ASCii
+(NSString *)stringToAscII:(NSString *)str{
    
    NSString *string = str;
    int asciiCode = [string characterAtIndex:0];
    return [NSString stringWithFormat:@"%d",asciiCode];
    
}

+ (NSArray *)readFile:(NSString *)fileName
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSString *content = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    content = [content stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    NSArray *arr = [content componentsSeparatedByString:@","];
    
    return arr;
    
}


+ (NSDictionary*)readTxtFile:(NSString*)fileName
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSString *content = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    content  = [content stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSLog(@"content:%@",content);
    
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *tempDictQueryDiamond = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"data:%@",tempDictQueryDiamond);
    
    return tempDictQueryDiamond;
    
    
}

//json字符串转化成OC键值对
+ (id)jsonStringToKeyValues:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = nil;
    if (JSONData) {
        responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    }
    
    return responseJSON;
}


//读取数组套字典的txt
+(NSArray *)tValueToArr:(NSString *)fileName{
    
    NSArray *tArr = [linkManager readFile:fileName];
    NSMutableArray *TmArr = @[].mutableCopy;
    
    for (NSString *tStr in tArr) {
        
        NSLog(@"tStr:%@",tStr);
        
        if (tStr.length!=0) {
            
            NSData *data = [tStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *tempDictQueryDiamond = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"tempDictQueryDiamond:%@",tempDictQueryDiamond);
            [TmArr addObject:tempDictQueryDiamond];
            
        }
        
    }
    
    NSLog(@"TmArr:%@",TmArr);
    return TmArr.mutableCopy;
    
}

//数组套数组
+(NSArray*)arrContainArr:(NSString *)fileName{
    
    NSArray *tArr = [linkManager readFile:fileName];
    NSMutableArray *TmArr = @[].mutableCopy;
    
    for (NSString *tStr in tArr) {
        
       NSString * content = [tStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        content = [content stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        content = [content stringByReplacingOccurrencesOfString:@"{" withString:@""];
        content = [content stringByReplacingOccurrencesOfString:@"}" withString:@""];
        
        NSArray *arr = [content componentsSeparatedByString:@":"];
        [TmArr addObject:arr];
        
    }
    
    NSLog(@"TmArr:%@",TmArr);
    return TmArr.mutableCopy;
    
}



@end
