//
//  numManager.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/21.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "numManager.h"

@implementation numManager
+(NSString *)hexToTen:(NSString *)getS{
 
    NSString *s1 = [getS substringWithRange:NSMakeRange(0, 1)];
    NSString *s2 = [getS substringWithRange:NSMakeRange(1, 1)];

    NSInteger tNum = 0;
    NSInteger tNum1 = [numManager stringToNum:s1];
    NSInteger tNum2 = [numManager stringToNum:s2];
    tNum = tNum1*16+tNum2;
    
    if (tNum<10) {
        return [NSString stringWithFormat:@"00%ld",tNum];

    }else
    if (tNum<100) {
        return [NSString stringWithFormat:@"0%ld",tNum];
        
    }else{
        
        return [NSString stringWithFormat:@"%ld",tNum];
    }
    
}


+(NSInteger)stringToNum:(NSString *)tempStr {
    
    NSInteger tm = 0;
    
    if ([tempStr isEqualToString:@"A"]) {
        tm = 10;
    }
    else if ([tempStr isEqualToString:@"B"]) {
        tm = 11;
    }
    else if ([tempStr isEqualToString:@"C"]) {
        tm = 12;
    }
    else if ([tempStr isEqualToString:@"D"]) {
        tm = 13;
    }
    else if ([tempStr isEqualToString:@"E"]) {
        tm = 14;
    }
    else if ([tempStr isEqualToString:@"F"]) {
        tm = 15;
    }else{
        tm = [tempStr integerValue];
    }
    return tm ;
}


@end
