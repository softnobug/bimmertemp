//
//  socketManager.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

typedef void(^resultBlock)(void);
typedef void(^receiveData)(NSString *recMes);


#import <Foundation/Foundation.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

@interface socketManager : NSObject<GCDAsyncSocketDelegate>
+(NSString *)localWiFiIPAddress;
+ (NSString *)getWifiName;

@end
