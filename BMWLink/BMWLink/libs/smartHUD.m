//
//  smartHUD.m
//  wheelservice
//
//  Created by 孟维学 on 2017/1/8.
//  Copyright © 2017年 wheelheres. All rights reserved.
//

#import "smartHUD.h"
#import "MBProgressHUD.h"

static MBProgressHUD *progerssHUD=nil;

@implementation smartHUD


+(void)alertText:(NSString *)message view:(UIView *)addview delay:(NSInteger)delaytime after:(void(^)())OK{
    
    MBProgressHUD *textHUD;
    textHUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];
    textHUD.mode = MBProgressHUDModeText;
    textHUD.label.text = message;
    textHUD.margin = 20.f;
    textHUD.removeFromSuperViewOnHide = YES;
//    [textHUD hide:YES afterDelay:delaytime];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaytime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        OK();
        
    });
    
    
}

+(void)alertText:(UIView *)view alert:(NSString *)alert delay:(NSInteger)delaytime{
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];

    hud.mode = MBProgressHUDModeText;
    hud.label.text = alert;
    
    [hud hideAnimated:YES afterDelay:delaytime];
    
//    MBProgressHUD *textHUD;
//    textHUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    textHUD.mode = MBProgressHUDModeText;
//    textHUD.labelText = alert;
//
//    textHUD.margin = 20.f;
//    textHUD.removeFromSuperViewOnHide = YES;
//    [textHUD hide:YES afterDelay:delaytime];
    
}

+(void)progressShow:(UIView *)view alertText:(NSString *)alert{

    if (progerssHUD == nil) {
        
        progerssHUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];
        progerssHUD.mode = MBProgressHUDModeIndeterminate;
        progerssHUD.label.text = alert;
        progerssHUD.removeFromSuperViewOnHide = YES;
        
    }
    
}

+(void)Hide{
    
    [progerssHUD hideAnimated:YES];
    progerssHUD = nil;
}

@end
