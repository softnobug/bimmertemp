//
//  careMessageFunctionViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "careMessageFunctionViewController.h"

@interface careMessageFunctionViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *carVinNum;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSArray *showcontentArr;

@end

@implementation careMessageFunctionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview registerNib:[UINib nibWithNibName:@"ISTATableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    [self.view addSubview:self.tableview];
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        
    }
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    
    self.showcontentArr = @[@"删除故障代码",
                            @"控制单元复位",
                            @"解除控制单元运输模式"];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _showcontentArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ISTATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.contentLa.text = _showcontentArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    [self sendMBData:(NSString *)];
    
    NSArray *indexArr = @[@"0414FFFFFF0000",
                          @"02110100000000",
                          @"0531010F0C0000"];
    
    [self sendMBData:indexArr[indexPath.row]];
    
}


-(void)sendMBData:(NSString *)message{
    
    NSArray *arr = [linkManager readFile:@"ISTA_top.txt"];
    NSMutableArray *mArr = @[].mutableCopy;
    
    for (NSString *ss in arr) {
        
//        NSString *tss = [ss stringByReplacingOccurrencesOfString:@"XX" withString:[self.keyStr substringWithRange:NSMakeRange(1, 2)]];
        
//        [mArr addObject:tss];
        
    }
    
    [mArr addObject:message];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ISTA" object:nil userInfo:@{@"ista":mArr}];
    
    NSLog(@"%@",mArr);
    
}

//等待中
+(void)showP{
    
//    mTitleLa.hidden = NO;
//    mTitleLa.text = @"正在读取中...";
//    //    [smartHUD Hide];
//    mTitleLa.textColor = [UIColor redColor];
//    //    [smartHUD progressShow:[UIView new] alertText:@"操作已完成!"];
    
    
}

+(void)hideP{
    
//    mTitleLa.text = @"读取成功";
//    [smartHUD alertText:view alert:@"操作已完成!" delay: 2];
//    mTitleLa.textColor = [UIColor greenColor];
    //    hasDone = YES;
}



- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)popAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
