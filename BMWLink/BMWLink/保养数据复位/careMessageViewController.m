//
//  careMessageViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "careMessageViewController.h"

@interface careMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *vinCarNum;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSArray *showcontentArr;

@end

@implementation careMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.vinCarNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    [self initTable];
    
    _showcontentArr = @[@"车辆检查复位",
                        @"法定车辆检查复位",
                        @"刹车制动液更换复位",
                        @"法定尾气检查信息复位",
                        @"机油更换复位",
                        @"前刹车片复位至5万公里",
                        @"后刹车片复位至5万公里"];
    [self showMessage];
}

-(void)showMessage{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"此功能会复位车辆各项保养信息.如未实际更换机油或刹车数据检测线,则复位后还会显示车载电脑当前实测数据." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:action1];
    
    [self presentViewController:alert animated:YES completion:nil];

    
}


-(void)initTable{
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ISTATableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
        
    }
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
//    mtableview = self.tableview;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _showcontentArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ISTATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.contentLa.text =  _showcontentArr[indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
//    careMessageFunctionViewController *care = [[careMessageFunctionViewController alloc]initWithNibName:@"careMessageFunctionViewController" bundle:nil];
//    care.vinNum = self.vinNum;
//    care.carPlat = self.carPlat;
//    [self.navigationController pushViewController:care animated:YES];
    
}
- (IBAction)backAtion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)disAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
