//
//  smartAFN.m
//  BMWLink
//
//  Created by weixue meng on 2018/9/17.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import "smartAFN.h"

@implementation smartAFN

static smartAFN *smartafn;

+(void)downLoadWithName:(NSString*)fileName success:(returnNon)done fail:(returnNon)fail{
    
    AFHTTPRequestOperation *operation1;
    
   NSString * urlStr = [NSString stringWithFormat:@"http://47.104.223.158/Esys-Web/getXmlById?name=%@.ncd",fileName];
    
    // 任务1
    operation1 = [LCDownloadManager downloadFileWithURLString:urlStr cachePath:[NSString stringWithFormat:@"%@.txt",fileName] progress:^(CGFloat progress, CGFloat totalMBRead, CGFloat totalMBExpectedToRead) {
        
        NSLog(@"progress: %.2f ", progress);
        //        self.progressView.progress = progress;
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        done();
        
        NSLog(@"Task1 -> Download finish");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        fail();
        if (error.code == -999) NSLog(@"Task1 -> Maybe you pause download.");
        
        NSLog(@"Task1 -> %@", error);
    }];
    
    
}



+(void)downLoadWithFileName:(NSString*)fileName  success:(returnNon)done fail:(returnNon)fail{
    
    
    AFHTTPRequestOperation *operation1;
    NSString * urlStr = @"http://www.car-lync.com/myorder/CAFD_0000187F_000_000_000.txt";
    // 任务1
    operation1 = [LCDownloadManager downloadFileWithURLString:urlStr cachePath:[NSString stringWithFormat:@"%@.txt",fileName] progress:^(CGFloat progress, CGFloat totalMBRead, CGFloat totalMBExpectedToRead) {
        
        NSLog(@"progress: %.2f ", progress);
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        done();
        NSLog(@"Task1 -> Download finish");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        fail();
        if (error.code == -999) NSLog(@"Task1 -> Maybe you pause download.");
        
        NSLog(@"Task1 -> %@", error);
    }];
    
    
}


+(void)smartAFNpost:(NSString *)url parameters:(NSDictionary *)parameter success:(void (^)(NSDictionary *dict))success wrong:(void(^)(NSDictionary *dict))wrong  sure:(void (^)(NSString *sure))sureme failure:(void (^)(NSError *))failure {
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //申明返回的结果是json类型
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",@"text/json",nil];
    
    //    [manager.securityPolicy setAllowInvalidCertificates:YES];
    
    //    [manager setSecurityPolicy:[self customSecurityPolicy]];
    
    
    //发送请求
    [manager POST:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        
        success(dict);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure(error);
        NSLog(@"网络问题");
        
    }];
    
    
}


+(void)smartAFNGet:(NSString *)url parameters:(NSDictionary *)parameter success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *))failure {
    
    
    //    Mlog(url);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //申明返回的结果是json类型
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //申明请求的数据是json类型
    manager.requestSerializer=[AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",@"text/json",nil];
    
    //    [manager.securityPolicy setAllowInvalidCertificates:YES];
    
    //    [manager setSecurityPolicy:[self customSecurityPolicy]];
    
    [manager GET:url parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        
        //        NSLog(@"请求成功:%@",dict);
        
        success(dict);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //        NSLog(@"失败:%@",error);
        
        failure(error);
        
    }];
    
    
    
    
}


@end
