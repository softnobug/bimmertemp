//
//  bluetoothlistVeiw.h
//  airdetector
//
//  Created by 孟维学 on 2016/12/29.
//  Copyright © 2016年 carlync. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface bluetoothlistVeiw : UIView<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableview;
@property(nonatomic,strong)UIView *backView;
@property(nonatomic,strong)NSArray *listArr;
@property(nonatomic,copy)void(^blueSel)(CBPeripheral * peripheral);
@end
