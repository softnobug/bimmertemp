//
//  carVinNumViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

typedef void(^vinBackBlock)(NSInteger m);

#import <UIKit/UIKit.h>

@interface carVinNumViewController : UIViewController
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@property(nonatomic,copy)vinBackBlock myblockL;
@property(nonatomic,copy)vinBackBlock myblockR;

+(void)hideP;
+(void)showP;

@end
