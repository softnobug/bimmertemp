//
//  CAFDViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/4/23.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "CAFDViewController.h"

@interface CAFDViewController ()
@property(nonatomic,assign)NSInteger m;
@end

@implementation CAFDViewController
static NSMutableArray *receArr;
static NSMutableArray *dropArr;
static NSString *fileName_part;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.m = 0;
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    receArr = @[].mutableCopy;
    fileName_part = self.fileName_part;
    
    [self downLoad];
    
}

-(void)downLoad{
    
    NSLog(@"subArr:%@",self.subArr);
    
    if (self.subArr.count!=0&&self.m<self.subArr.count) {
        
        [smartAFN downLoadWithName:self.subArr[self.m] success:^{
            
            NSLog(@"++");

            self.m++;
            [self downLoad];
            
            if (self.m==self.subArr.count-1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"--------");
                });
                
            }
            
        } fail:^{
            
            NSLog(@"-fail------");

        }];
        
    }
    
}




+(void)receIveToCreatData:(NSString *)receStr{
    
    NSArray *Narr = [receStr componentsSeparatedByString:@"\n"];
    
    if (Narr.count==1&&receStr.length>30) {
        Narr = [receStr componentsSeparatedByString:@"\r"];
    }
    
    NSMutableArray *arr= @[].mutableCopy;
    
    for (NSString *ts in Narr) {
        
        NSString *temptext = [ts stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *tts = [temptext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (tts.length > 10) {
            tts = [tts substringFromIndex:5];
            [arr addObject:tts];
        }
    }
    
    
    if (arr.count!=0) {
        
        [receArr addObject:arr];
        
    }
    
    NSLog(@"receArr:%@",receArr);
    NSDate *date=[NSDate date];
    NSDateFormatter *format1=[[NSDateFormatter alloc] init];
    [format1 setDateFormat:@"yyyy_MM_dd_hh_mm"];
    NSString *dateStr;
    dateStr=[format1 stringFromDate:date];
    [FileManager writeData:receArr fileName:[NSString stringWithFormat:@"%@.txt",fileName_part]];
    
}

//隐藏
+(void)hideP{
   
    if (dropArr) {
        [dropArr removeAllObjects];
    }else{
        dropArr = @[].mutableCopy;

    }
    
    [dropArr addObject:@"AT ST ff"];
    [dropArr addObject:@"021001"];
    [dropArr addObject:@"021003"];
    [dropArr addObject:@"021041"];
    [dropArr addObject:@"062701FFFFFFFF"];
    
    
    for (int i = 0; i<receArr.count; i++) {
        
        NSArray *temArr = receArr[i];
        
        if (i ==receArr.count-1) {
            
            [dropArr addObject:@"0431010F01"];
            [dropArr addObject:@"023E80 0"];
            
            for (int j = 0; j< temArr.count; j++) {
                
                NSString *temStr = temArr[j];
                
                if (j==temArr.count-1) {
                    
                    [dropArr addObject:temStr];
                    [dropArr addObject:@"021101"];
                    
                }else{
                    
                    if ([temStr hasPrefix:@"1"]) {
                        
                        temStr = [NSString stringWithFormat:@"%@ 1",temStr];
                        
                    }else{
                        
                        temStr = [NSString stringWithFormat:@"%@ 0",temStr];
                        
                    }
                    
                    [dropArr addObject:temStr];
                }
                
            }
            
        }else{
            
            for (int j = 0; j< temArr.count; j++) {
                
                NSString *temStr = temArr[j];
                
                if (j==temArr.count-1) {
                    
                    [dropArr addObject:temStr];
                    [dropArr addObject:@"023E80 0"];
                    
                }else{
                    
                    if ([temStr hasPrefix:@"1"]) {
                        temStr = [NSString stringWithFormat:@"%@ 1",temStr];
                        
                    }else{
                        temStr = [NSString stringWithFormat:@"%@ 0",temStr];
                    }
                    
                    [dropArr addObject:temStr];
                }
                
            }
            
        }
        
    }
    
    NSLog(@"dropArr:%@",dropArr);
    
}


//收值CAFD
- (IBAction)sendCAFD:(id)sender {
    
    [self sendCAFData];
    
}

//发值CAFD
- (IBAction)sendCAFDAction:(id)sender {
    
    NSArray *arr = [linkManager readFile:@"tcafd.txt"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"cafdsend" object:nil userInfo:@{@"ista":arr}];
    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"cafdsend" object:nil userInfo:@{@"ista":dropArr}];
    
}


-(void)sendCAFData{
    
    NSArray *arr = [linkManager readFile:@"CAFD_00001EF6_006_020_007.txt"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"cafdFirst" object:nil userInfo:@{@"ista":arr}];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)disAction:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
