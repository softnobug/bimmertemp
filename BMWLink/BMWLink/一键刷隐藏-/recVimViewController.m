//
//  recVimViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/17.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "recVimViewController.h"

@interface recVimViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *titleLa;

//@property(nonatomic,strong)NSMutableArray *contentArr;
@end

@implementation recVimViewController
static NSMutableArray *contentArr;
static NSMutableArray *showcontentArr;
static NSMutableArray *showSubcontentArr;
static NSMutableArray *showTitContentArr;

static UIView *view;
static UILabel *mTitleLa;

static UITableView *mtableview;
static NSMutableArray *subArr;
static NSDictionary *mtitDic;
static  BOOL hasDone;

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    view = self.view;
    mTitleLa.text = @"正在读取中...";
    mTitleLa.textColor = [UIColor redColor];
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    contentArr = @[].mutableCopy;
    showcontentArr = @[].mutableCopy;
    showSubcontentArr = @[].mutableCopy;
    showTitContentArr = @[].mutableCopy;

    [self initView];
    [self contentSubArr];
    mTitleLa = self.titleLa;
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.vinAndCarPlatLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
}

-(void)contentSubArr{
    
//   NSArray * =[linkManager readFile:@"full.txt"];
    
}

+(void)getTitleArr:(NSDictionary*)titDic{
    
    mtitDic = titDic;
    
}

+(void)addDataStr:(NSString *)arrStr{
    
    NSArray *Narr = [arrStr componentsSeparatedByString:@"\n"];
    
    if (Narr.count==1&&arrStr.length>30) {
        Narr = [arrStr componentsSeparatedByString:@"\r"];
    }
    
    NSMutableArray *arr= @[].mutableCopy;
    
    for (NSString *ts in Narr) {
        
        NSString *tts = [ts  stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        tts = [ts stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        
        if (tts.length==19) {
            
            [arr addObject:tts];
            NSLog(@"rec--");
            
        }else{
            
            NSLog(@"rec--%ld",tts.length);
        }
        
    }
    
    NSLog(@"ASD:%@",arr);
    
    if (arr.count!=0) {
        
        [contentArr addObject:arr];
        [recVimViewController getTitleName:arr];
        
    }
    
}


+(void)getTitleName:(NSArray *)tArr{
    
    NSString *sub = [tArr[0] substringWithRange:NSMakeRange(0, 3)];
    NSString *titleN = [mtitDic objectForKey:sub];
    
    NSString *subEndStr = [recVimViewController setSubTitle:tArr];
    NSDictionary *tA = @{titleN:subEndStr};
    
    NSLog(@"mtA:%@",tA);
    
    [showTitContentArr addObject:titleN];
    [showSubcontentArr addObject:subEndStr];
    
    [showcontentArr addObject:tA];
    
    NSMutableArray *subArrT = @[].mutableCopy;
    
    subArrT = [ChineseString LetterSortArray:showTitContentArr.mutableCopy];
    
    NSLog(@"subArrT:%@",subArrT);
    
    NSMutableArray *tShowArr = @[].mutableCopy;
    
    for (NSArray *tArr in subArrT) {
        
        for (NSString *ts in tArr) {
            
            [tShowArr addObject:ts];
            
        }
        
    }
    
    [showSubcontentArr removeAllObjects];
    for (NSString *tS in showTitContentArr) {
        
        for (NSDictionary *dict in showcontentArr) {
            
            if ([dict.allKeys[0] isEqualToString:tS]) {
                [showSubcontentArr addObject:[dict objectForKey:tS]];
                
            }
            
        }
        
    }
    
    NSLog(@"showcontentArr:%@",showcontentArr);
    
    [showTitContentArr removeAllObjects];
    [showTitContentArr addObjectsFromArray:tShowArr.mutableCopy];
    
    [mtableview reloadData];
    
    
}

+(NSString *)setSubTitle:(NSArray*)tArr{
    
    NSString *sub = [tArr[0] substringWithRange:NSMakeRange(0, 3)];
    
    NSArray *comParArr = [linkManager tValueToArr:[NSString stringWithFormat:@"%@.txt",sub]];
    
    NSString *subEndStr = @"";
    NSMutableArray *subEndArr = @[].mutableCopy;
    
    for (int i = 0; i<tArr.count; i++) {
        
        NSString *titStr = [tArr[i]substringWithRange:NSMakeRange(0, 7)];
        
        if (i<comParArr.count && i<tArr.count-1) {
            
            NSDictionary *Tdict = comParArr[i];
            NSString *sindex = [Tdict objectForKey:titStr];
            NSString *SubS = [recVimViewController subTitlePinFrom:tArr[i] indexStr:sindex];
            
            if (SubS.length != 0) {
                
                NSString *ls = @"";
                if ([sindex integerValue]<6) {
                    
                    ls = [NSString stringWithFormat:@"%@%@",[tArr[i] substringWithRange:NSMakeRange(7,[tArr[i] length]-7)],[tArr[i+1] substringWithRange:NSMakeRange(7,[tArr[i] length]-7)]];
                    
                }else{
                    
                    ls = [NSString stringWithFormat:@"%@%@%@",[tArr[i] substringWithRange:NSMakeRange(7,[tArr[i] length]-7)],[tArr[i+1] substringWithRange:NSMakeRange(7,[tArr[i] length]-7)],[tArr[i+2] substringWithRange:NSMakeRange(7,[tArr[i] length]-7)]];
                    
                }
                
                
                NSString *s1 = [NSString stringWithFormat:@"%@",[ls substringWithRange:NSMakeRange([sindex integerValue]*2, 8)]];
                
                NSString *s2 = [NSString stringWithFormat:@"%@",[ls substringWithRange:NSMakeRange([sindex integerValue]*2+8, 2)]];
                NSString *s3 = [NSString stringWithFormat:@"%@",[ls substringWithRange:NSMakeRange([sindex integerValue]*2+10, 2)]];
                NSString *s4 = [NSString stringWithFormat:@"%@",[ls substringWithRange:NSMakeRange([sindex integerValue]*2+12, 2)]];
                
                s2 = [numManager hexToTen:s2];
                s3 = [numManager hexToTen:s3];
                s4 = [numManager hexToTen:s4];
                
                NSString *finalS = [NSString stringWithFormat:@"%@_%@_%@_%@_%@",SubS,s1,s2,s3,s4];

                NSLog(@"mfS:%@",subEndStr);
                [subEndArr addObject:finalS];
                
            }
            
        }
        
    }
    
    
    NSMutableArray *subArrT = @[].mutableCopy;
    
    subArrT = [ChineseString LetterSortArray:subEndArr.mutableCopy];
    
    NSLog(@"subArrT:%@",subArrT);
    
    for (NSArray *tArr in subArrT) {
        
        for (NSString *ts in tArr) {
            
            subEndStr = [NSString stringWithFormat:@"%@\n%@",subEndStr,ts];

        }
        
    }
    
    
//    NSLog(@"subD:%@",comParArr);
    
    return subEndStr;
    
}

+(NSString*)subTitlePinFrom:(NSString *)fromStr indexStr:(NSString*)index{
    
    if ([index integerValue]==9) {
        
        NSLog(@"等于9");
        
        return @"";
        
    }
    
    NSLog(@"不等于9");

    NSString *subStr = [fromStr substringWithRange:NSMakeRange(([index integerValue]-1)*2+7, 2)];
    NSDictionary *dict = [linkManager readTxtFile:@"list_name.txt"];
    
    if ([[dict allKeys]containsObject:subStr]) {
        
        NSString *finalStr = [dict objectForKey:subStr];
        NSLog(@"finalStrsubStr:%@",subStr);
        NSLog(@"finaldict:%@",dict);
        NSLog(@"finalStr:%@",finalStr);
        
        return finalStr;
        
    }else{
        
        return @"";
    }
    
}


-(void)initView{
    
    mtableview = self.tableview;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"stateVinTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
        
    }
    
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return showcontentArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    stateVinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    NSDictionary *titD =showcontentArr[indexPath.row];
    NSString *titleStr = showTitContentArr[indexPath.row];
    cell.titleLa.text = titleStr;
    
    cell.contentLa.text = showSubcontentArr[indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *subStr = showSubcontentArr[indexPath.row];

    if ([subStr containsString:@"CAFD"]) {

        CAFDViewController *recVC = [[CAFDViewController alloc]initWithNibName:@"CAFDViewController" bundle:nil];
        recVC.vinNum = self.vinNum;
        recVC.carPlat = self.carPlat;
        
        recVC.fileName_part = [showSubcontentArr[indexPath.row] substringWithRange:NSMakeRange(0, 26)];
        recVC.subArr = [self cafdArr:showSubcontentArr[indexPath.row]];
        
        NSLog(@"%@",showSubcontentArr[indexPath.row]);
        
        [[NSUserDefaults standardUserDefaults]setObject:showSubcontentArr[indexPath.row] forKey:@"faf"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self.navigationController pushViewController:recVC animated:YES];
        
    }
    
}




-(NSArray*)cafdArr:(NSString*)mstr{
    
    NSMutableArray *titleArr = @[].mutableCopy;
    
    for (int i = 0; i<mstr.length/26; i++) {
        
        if ([[mstr substringWithRange:NSMakeRange(i*26, 26)]containsString:@"CAFD"]) {
            [titleArr addObject:[mstr substringWithRange:NSMakeRange(i*26+1, 26-1)]];
            
        }
        
    }
    
    return titleArr.mutableCopy;
    
}


-(void)getCAFD:(NSString*)mstr{
    
    
    
}




- (IBAction)disConAction:(id)sender {
    
    if (hasDone) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [smartHUD alertText:self.view alert:@"数据传输中,请稍后." delay:2];
    }
    
}

- (IBAction)popAction:(id)sender {
    
    if (hasDone) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [smartHUD alertText:self.view alert:@"数据传输中,请稍后." delay:2];
    }
    
}

+(void)addData:(NSArray*)arr{
    
    [mtableview reloadData];
    
}

//等待中
+(void)showP{
    
    
    mTitleLa.text = @"正在读取中...";
    mTitleLa.textColor = [UIColor redColor];
    
}

+(void)hideP{
    
    [smartHUD Hide];
    mTitleLa.text = @"读取成功";
    mTitleLa.textColor = [UIColor greenColor];
    hasDone = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
