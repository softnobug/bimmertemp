//
//  CAFDViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/4/23.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAFDViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *carVinNum;
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@property(nonatomic,strong)NSString *fileName_part;
@property(nonatomic,strong)NSArray *subArr;

+(void)receIveToCreatData:(NSString *)receStr;
+(void)hideP;
+(void)showP;


@end
