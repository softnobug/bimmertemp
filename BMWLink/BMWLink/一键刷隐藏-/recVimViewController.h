//
//  recVimViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/17.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface recVimViewController : UIViewController
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@property (weak, nonatomic) IBOutlet UILabel *vinAndCarPlatLa;

+(void)addData:(NSArray*)arr;
+(void)addDataStr:(NSString*)arrStr;
+(void)getTitleArr:(NSDictionary*)titDic;
+(void)hideP;
+(void)showP;
@end
