//
//  bsxNextViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "bsxNextViewController.h"

@interface bsxNextViewController ()
@property (weak, nonatomic) IBOutlet UILabel *carVinNum;
@property (weak, nonatomic) IBOutlet UILabel *leftA;
@property (weak, nonatomic) IBOutlet UILabel *leftB;
@property (weak, nonatomic) IBOutlet UILabel *leftC;
@property (weak, nonatomic) IBOutlet UILabel *leftD;
@property (weak, nonatomic) IBOutlet UILabel *leftE;

@property (weak, nonatomic) IBOutlet UILabel *rightA;
@property (weak, nonatomic) IBOutlet UILabel *rightB;
@property (weak, nonatomic) IBOutlet UILabel *rightC;
@property (weak, nonatomic) IBOutlet UILabel *rightD;
@property (weak, nonatomic) IBOutlet UILabel *rightE;
@property (weak, nonatomic) IBOutlet UIImageView *curImage;
//@property(nonatomic,assign)BOOL iswrite;

@end

@implementation bsxNextViewController
static BOOL iswrite;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
    switch (self.fromWho) {
        case 0:
            self.curImage.image = [UIImage imageNamed:@"bmw.jpg"];
            break;
        case 1:
            self.curImage.image = [UIImage imageNamed:@"M.jpg"];
            break;
        case 3:
            self.curImage.image = [UIImage imageNamed:@"DTE.png"];
            break;
        case 2:
            self.curImage.image = [UIImage imageNamed:@"TMC.png"];
            break;
        default:
            break;
    }
    
    [self setnum];
    
}

-(void)setnum{
    
    self.leftA.text = @"0.0";
    self.leftB.text = @"0.0";
    self.leftC.text = @"0.0";
    self.leftD.text = @"0.0";
    self.leftE.text = @"0.0";
    self.rightA.text = @"0.0";
    self.rightB.text = @"0.0";
    self.rightC.text = @"0.0";
    self.rightD.text = @"0.0";
    self.rightE.text = @"0.0";
    
    
}



//读取当前车辆数据
- (IBAction)readAction:(id)sender {
    
    iswrite = NO;
    NSArray *mArr = @[@"AT ST 19",
                      @"AT FC SH 6F1",
                      @"AT FC SD 18 30 FF 08",
                      @"AT CRA 618",
                      @"AT CEA 18",
                      @"0322413A000000",
                      @"300F0200000000"
                      ];
    
    //收到指定值后,立即发送
//    NSArray *mArr = @[@"03224140000000",
//                      @"300F0200000000"
//                      ];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"bsxread" object:nil userInfo:@{@"ista":mArr}];
    
    
//    [ViewController sockerFunc:mArr[0] receive:^(NSString *recStr) {
//
//        NSLog(@"新收值:%@",recStr);
//
//    } start:^{
//        NSLog(@"新开始");
//
//    } end:^{
//        NSLog(@"新结束");
//    }];
//
    
    
}

//等待中
+(void)showP{
    
    if (iswrite) {
        
        [smartHUD progressShow:[UIView new] alertText:@"程序写入中,请稍后..."];
        
    }else{
        
        [smartHUD progressShow:[UIView new] alertText:@"车辆数据读取中,请稍后..."];
        
    }
    
}

+(void)hideP{
    
    if (iswrite) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [smartHUD Hide];
            [smartHUD alertText:[UIView new] alert:@"写入完成!" delay:1];

        });
    }else{
        [smartHUD Hide];
    }
    
}


//升级程序写入变速箱复位
- (IBAction)updateAction:(id)sender {
    
    iswrite = YES;

    
    NSArray *mArr = @[@"AT ST ff",
                      @"AT FC SH 6F1",
                      @"AT FC SD 18 30 FF 08",
                      @"AT FC SM1",
                      @"AT CRA 618",
                      @"AT CEA 18",
                      @"042E4150000000"
                      ];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"bsxwrite" object:nil userInfo:@{@"ista":mArr}];
    
}


- (IBAction)backActiion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)disAction:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
