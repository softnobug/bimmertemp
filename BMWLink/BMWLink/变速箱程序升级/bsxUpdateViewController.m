//
//  bsxUpdateViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "bsxUpdateViewController.h"

@interface bsxUpdateViewController ()
@property (weak, nonatomic) IBOutlet UILabel *carVinNum;

@end

@implementation bsxUpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
    
}

- (IBAction)actionOne:(id)sender {
    
    bsxNextViewController *oneStep = [[bsxNextViewController alloc]initWithNibName:@"bsxNextViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
    oneStep.fromWho = 0;
    [self.navigationController pushViewController:oneStep animated:YES];
    
}
- (IBAction)actionTwo:(id)sender {
    bsxNextViewController *oneStep = [[bsxNextViewController alloc]initWithNibName:@"bsxNextViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
     oneStep.fromWho = 1;
    [self.navigationController pushViewController:oneStep animated:YES];
}

- (IBAction)actionThree:(id)sender {
    bsxNextViewController *oneStep = [[bsxNextViewController alloc]initWithNibName:@"bsxNextViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
     oneStep.fromWho = 2;
    [self.navigationController pushViewController:oneStep animated:YES];
}
- (IBAction)actionFour:(id)sender {
    bsxNextViewController *oneStep = [[bsxNextViewController alloc]initWithNibName:@"bsxNextViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
     oneStep.fromWho = 3;
    [self.navigationController pushViewController:oneStep animated:YES];
    
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)disAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
