//
//  bsxNextViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>
//bsxwrite.txt
@interface bsxNextViewController : UIViewController
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@property(nonatomic,assign)NSInteger fromWho;
+(void)hideP;
+(void)showP;

@end
