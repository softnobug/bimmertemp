//
//  smartAFN.h
//  BMWLink
//
//  Created by weixue meng on 2018/9/17.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#define VIDEO_URL @"http://47.104.223.158/Esys-Web/getXmlById?name=CAFD_000000D0_003_004_004.ncd"

typedef void(^returnNon)(void);

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>


@interface smartAFN : NSObject


+(void)downLoadWithName:(NSString*)fileName success:(returnNon)done fail:(returnNon)fail;
+(void)downLoadWithFileName:(NSString*)fileName  success:(returnNon)done fail:(returnNon)fail;
//post
+(void)smartAFN:(NSString *)url parameters:(NSDictionary *)parameter success:(void (^)(NSDictionary *dict))success  failure:(void (^)(NSError *error))failure;

//get
+(void)smartAFNGet:(NSString *)url parameters:(NSDictionary *)parameter success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure;

@end
