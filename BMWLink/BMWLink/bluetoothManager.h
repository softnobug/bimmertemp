//
//  bluetoothManager.h
//  sassetup
//
//  Created by weixuemeng on 2018/4/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

typedef void(^discoverBlueBlock)(NSArray *blueArr);
typedef void(^connectBlock)(NSInteger m);
typedef void(^receDataBlock)(NSString *message);


#import <Foundation/Foundation.h>

@interface bluetoothManager : NSObject<CBCentralManagerDelegate,CBPeripheralDelegate,CBPeripheralManagerDelegate>

@property(nonatomic,strong)CBCentralManager *cbCentralMgr;//中心
@property(nonatomic,strong)NSMutableArray *peripheralArray;
@property(nonatomic,strong)UILabel *label;
@property(nonatomic,strong)CBPeripheral *peripheral;
@property (nonatomic, strong) CBPeripheralManager *peripheralManager;//周边
@property (nonatomic, strong) CBMutableCharacteristic *customCharacteristic;//特征
@property (nonatomic, strong) CBMutableService *customService;//服务
@property(nonatomic,strong)CBCharacteristic *characteristic;
@property(nonatomic,strong)NSMutableData *recvData;//接收到的数据
@property(nonatomic,strong)NSString *myValue;
@property(nonatomic,strong)id<CBPeripheralManagerDelegate> delegate;
@property(nonatomic,copy)discoverBlueBlock disBlock;
@property(nonatomic,copy)connectBlock conBlock;
@property(nonatomic,copy)receDataBlock recBlock;
@property(nonatomic,assign)BOOL hasConnected;
@property(nonatomic,assign)BOOL hasAuth;

-(void)initObects;
-(void)scanBluetooth;
-(void)connectAction:(CBPeripheral * )peripheral;
-(void)receiveData;
-(void)sendMessage:(NSString *)message;

+(bluetoothManager*)shareInstance;


@end
