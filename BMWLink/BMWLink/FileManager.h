//
//  FileManager.h
//  BMWLink
//
//  Created by weixue meng on 2018/9/13.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
+ (NSString*)filePath:(NSString*)fileName; 
+(void)writeData:(NSArray*)arr fileName:(NSString *)fileName;//写入数据
+(NSArray *)readTxt:(NSString*)fileName;//读取数据
+(NSDictionary*)readXML:(NSString*)fileName;
+(NSArray*)myQuickMethod:(NSString*)fileJName;

+(NSDictionary *)readDownload:(NSString *)name path:(NSString *)path;

@end
