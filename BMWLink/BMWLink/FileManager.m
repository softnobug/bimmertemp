//
//  FileManager.m
//  BMWLink
//
//  Created by weixue meng on 2018/9/13.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (NSString*)filePath:(NSString*)fileName {
    
    NSArray* myPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* myDocPath = [myPaths objectAtIndex:0];
    
    myDocPath = [NSString stringWithFormat:@"%@/cafdraw/",myDocPath];
    
    NSString* filePath = [myDocPath stringByAppendingPathComponent:fileName];
    
    
    return filePath;
    
}


+(void)writeData:(NSArray*)arr fileName:(NSString *)fileName{
    
    // 字符串写入沙盒
    // 在Documents下面创建一个文本路径，假设文本名称为objc.txt
    NSString *txtPath = [FileManager filePath:fileName]; // 此时仅
    NSLog(@"txtPath:%@",txtPath);
    
    [arr writeToFile:txtPath atomically:YES];
    NSArray *resultArr1= [NSArray arrayWithContentsOfFile:txtPath];
    
    NSLog(@"fileName:%@",fileName);
    NSLog(@"resultArr1 is %@", resultArr1);
    


}

+(NSArray *)readTxt:(NSString*)fileName{
    

    NSError *error;
    NSString *txtPath = [FileManager filePath:fileName];
    
    NSArray *resultArr1= [NSArray arrayWithContentsOfFile:txtPath];
    
//    NSArray *resultArr = [NSArray arrayWithContentsOfFile:[FileManager filePath:fileName]];
    // 字符串读取的方法
//    NSString *resultStr = [NSString stringWithContentsOfFile:[FileManager filePath:fileName] encoding:NSUTF8StringEncoding error:nil];
//    NSLog(@"resultStr is %@", resultStr);
    
    return resultArr1;
}

+(NSDictionary*)readXML:(NSString*)fileName{
    
    NSError *error;
    
    NSArray* myPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString* myDocPath = [myPaths objectAtIndex:0];

    NSString*  txtPath =[NSString stringWithFormat:@"%@/ncd/%@.txt",myDocPath,fileName];
        
    NSString *content = [[NSString alloc] initWithContentsOfFile:txtPath encoding:NSUTF8StringEncoding error:&error];
    NSDictionary *xmlContent = [NSDictionary dictionaryWithXMLString:content];
    
    NSLog(@"xmlContent--------%@",xmlContent);
    return xmlContent;
    
}

//使用快速枚举来实现
+(NSArray*)myQuickMethod:(NSString*)fileJName{
    
    //文件操作对象
    NSFileManager *manager = [NSFileManager defaultManager];
    
    NSArray* myPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* myDocPath = [myPaths objectAtIndex:0];
    NSString*  txtPath =[NSString stringWithFormat:@"%@/%@",myDocPath,fileJName];
    
    NSLog(@"%@",txtPath);
    
    //目录迭代器
    NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:txtPath];
    //快速枚举
    NSMutableArray *files = [NSMutableArray arrayWithCapacity:42];
    for (NSString *filename in direnum) {
        
        if ([[filename pathExtension] isEqualToString:@"txt"]) {
            [files addObject:filename];
        }
    }
    
    //快速枚举，输出结果
    for (NSString *filename in files) {
       NSDictionary *dic =  [FileManager readDownload:filename path:txtPath];
        NSLog(@"%@==%@",filename,dic);

    }
    
    return files.mutableCopy;
    
}


+(NSDictionary *)readDownload:(NSString *)name path:(NSString *)path{
    
    NSString *mpath = [NSString stringWithFormat:@"%@/%@",path,name];
    NSError *error;
    NSString *content = [[NSString alloc] initWithContentsOfFile:mpath encoding:NSUTF8StringEncoding error:&error];
    NSDictionary *xmlContent = [NSDictionary dictionaryWithXMLString:content];
    
    return xmlContent;
    
}



@end
