//
//  carVinNumViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "carVinNumViewController.h"
#import "IBLabel.h"
@interface carVinNumViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSArray *contentArr;
@property (weak, nonatomic) IBOutlet IBLabel *vimNumLa;
@property (weak, nonatomic) IBOutlet UILabel *vinAndCarPlatLa;

@end

@implementation carVinNumViewController
static UIView *view;

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = YES;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    view = self.view;
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"doubleActTableViewCell" bundle:nil] forCellReuseIdentifier:@"dcell"];
    
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.vimNumLa.text = self.vinNum;
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.vinAndCarPlatLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    self.tableview.backgroundColor = [UIColor clearColor];
//    self.tableview.bounces = NO;
    
    self.contentArr = @[
                        @[@[@"dd1",@"一键刷隐藏"],@[@"dd2",@"ESYS高级刷隐藏"]],
                        @[@[@"dd3.png",@"ISTA+功能调用"],@[@"dd8.png",@"保养信息修改"]],
                        @[@[@"d9.png",@"快速删除故障码"],@[@"d10.png",@"变速箱程序升级"]],
                        @[@[@"dd7.png",@"数据库更新"],@[@"dd6.png",@"设置"]]
                        ];
    
//    ISTA+硬件诊断
//    实际里程查询
    
}

//等待中
+(void)showP{
    
    [smartHUD progressShow:view alertText:@"正在清除故障码!"];
    
}

+(void)hideP{
    
    [smartHUD Hide];
    
    [smartHUD alertText:view alert:@"故障码已清除!" delay:1];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.contentArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    doubleActTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dcell"];
    cell.backgroundColor = [UIColor clearColor];
    
    [cell.btn1 setImage:[UIImage imageNamed:self.contentArr[indexPath.row][0][0]] forState:UIControlStateNormal];
    [cell.btn2 setImage:[UIImage imageNamed:self.contentArr[indexPath.row][1][0]] forState:UIControlStateNormal];
    
    cell.titleLa1.text = self.contentArr[indexPath.row][0][1];
    cell.titleLa2.text = self.contentArr[indexPath.row][1][1];
    
    NSInteger mrow = indexPath.row;
    __weak typeof (self) weakself = self;
    

    cell.res1 = ^{
        
        if (indexPath.row == 0) {
            weakself.myblockL(mrow);
            [weakself oneStep];
        }
        else if (indexPath.row == 1) {
            weakself.myblockL(mrow);
            [self startISTA:@"F"];
        }
        else if (indexPath.row == 2){
            weakself.myblockL(mrow);
        }
        else if (indexPath.row == 3) {
//            weakself.myblockL(mrow);
            [self startISTA:@"Y"];
        }
        
    };
    
    cell.res2 = ^{
        
        if (mrow == 0) {
            
//            CAFDViewController *recVC = [[CAFDViewController alloc]initWithNibName:@"CAFDViewController" bundle:nil];
//            recVC.vinNum = self.vinNum;
//            recVC.carPlat = self.carPlat;
//            [self.navigationController pushViewController:recVC animated:YES];

            weakself.myblockR(mrow);
            [self startESYS];
            
        }
        else if (indexPath.row == 1){
            [self careMessage];
        }
        else if (indexPath.row == 2)
        {
            weakself.myblockR(mrow);
            
            [self bsxUpdate];
            
        }
        
        else if (indexPath.row == 3){
            [self setting];
        }
    };
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}


-(void)setting{
    settingViewController *setVC = [[settingViewController alloc]initWithNibName:@"settingViewController" bundle:nil];
    UINavigationController *setNC = [[UINavigationController alloc]initWithRootViewController:setVC];
    [self presentViewController:setNC animated:YES completion:nil];
    
}

//变速箱程序升级
-(void)bsxUpdate{
    
    bsxUpdateViewController *oneStep = [[bsxUpdateViewController alloc]initWithNibName:@"bsxUpdateViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
    [self.navigationController pushViewController:oneStep animated:YES];
}


//里程查询
-(void)bcCX{
    
    BCViewController *oneStep = [[BCViewController alloc]initWithNibName:@"BCViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
    [self.navigationController pushViewController:oneStep animated:YES];
}

//保养服务复位
-(void)careMessage{
    
    careMessageViewController *oneStep = [[careMessageViewController alloc]initWithNibName:@"careMessageViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
    [self.navigationController pushViewController:oneStep animated:YES];
    
    
}


//一键刷隐藏
-(void)oneStep{
    
//
//    oneStepHiddenNewViewController *oneStep = [[oneStepHiddenNewViewController alloc]initWithNibName:@"oneStepHiddenNewViewController" bundle:nil];
//    oneStep.vinNum = self.vinNum;
//    oneStep.carPlat = self.carPlat;
//    [self.navigationController pushViewController:oneStep animated:YES];
    
    oneStepHiddenViewController *oneStep = [[oneStepHiddenViewController alloc]initWithNibName:@"oneStepHiddenViewController" bundle:nil];
    oneStep.vinNum = self.vinNum;
    oneStep.carPlat = self.carPlat;
    [self.navigationController pushViewController:oneStep animated:YES];
    
    
}


-(void)startISTA:(NSString*)FOY{
    
    ISTAViewController *istaVC = [[ISTAViewController alloc]initWithNibName:@"ISTAViewController" bundle:nil];
    istaVC.titleStr = FOY;
    istaVC.vinNum = self.vinNum;
    istaVC.carPlat = self.carPlat;
    [self.navigationController pushViewController:istaVC animated:YES];
    
}

-(void)startESYS{
    
    recVimViewController *recVC = [[recVimViewController alloc]initWithNibName:@"recVimViewController" bundle:nil];
    recVC.vinNum = self.vinNum;
    recVC.carPlat = self.carPlat;
    [self.navigationController pushViewController:recVC animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 10;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}
- (IBAction)didConnect:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
