//
//  BCViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/31.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "BCViewController.h"

@interface BCViewController ()
@property (weak, nonatomic) IBOutlet UILabel *carVinNum;
@property (weak, nonatomic) IBOutlet IBLabel *yibiaoBC;
@property (weak, nonatomic) IBOutlet IBLabel *Fdjbc;
@property (weak, nonatomic) IBOutlet IBLabel *computrebc;

@end

@implementation BCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinNum.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
    
    self.yibiaoBC.text = @"14215 km";
    self.Fdjbc.text = @"56789 km";
    self.computrebc.text = @"56789 km";
    
}
//开始检测

- (IBAction)startAction:(id)sender {
}


- (IBAction)bacAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)disAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
