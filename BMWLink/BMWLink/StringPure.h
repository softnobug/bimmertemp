//
//  StringPure.h
//  BMWLink
//
//  Created by weixue meng on 2018/9/13.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringPure : NSObject
+(NSString*)stringPure:(NSString*)orginStr;

@end
