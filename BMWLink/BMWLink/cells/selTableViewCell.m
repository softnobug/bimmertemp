//
//  selTableViewCell.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "selTableViewCell.h"

@implementation selTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)selAction:(id)sender {
    
    self.mblck(self.isSel);
    
}

-(void)setSelState:(BOOL)hasSel{
    
    if (!hasSel) {
        [self.selBtn setImage:[UIImage imageNamed:@"V0"] forState:UIControlStateNormal];
    }else{
        
        [self.selBtn setImage:[UIImage imageNamed:@"V1"] forState:UIControlStateNormal];
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
