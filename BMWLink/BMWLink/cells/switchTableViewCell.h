//
//  switchTableViewCell.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/15.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//
typedef void(^switchB)(BOOL ison);

#import <UIKit/UIKit.h>

@interface switchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switchB;
@property(nonatomic,copy)switchB swib;
@end
