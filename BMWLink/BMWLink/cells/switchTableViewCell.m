//
//  switchTableViewCell.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/15.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "switchTableViewCell.h"

@implementation switchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.switchB addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)switchAction:(UISwitch*)sender{
    
    self.swib(sender.on);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
