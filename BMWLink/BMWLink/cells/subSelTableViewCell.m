//
//  subSelTableViewCell.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/27.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "subSelTableViewCell.h"

@implementation subSelTableViewCell

- (IBAction)selAction:(id)sender {
    
//    self.isSel = !self.isSel;

    self.mblck(self.isSel);
    
//    if (!self.isSel) {
//        [self.selBtn setImage:[UIImage imageNamed:@"V0"] forState:UIControlStateNormal];
//    }else{
//
//        [self.selBtn setImage:[UIImage imageNamed:@"V1"] forState:UIControlStateNormal];
//    }
    
}

-(void)setSelState:(BOOL)hasSel{
    
    if (!hasSel) {
        [self.selBtn setImage:[UIImage imageNamed:@"V0"] forState:UIControlStateNormal];
    }else{
        
        [self.selBtn setImage:[UIImage imageNamed:@"V1"] forState:UIControlStateNormal];
    }
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
