//
//  doubleActTableViewCell.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "doubleActTableViewCell.h"

@implementation doubleActTableViewCell

- (IBAction)btn1Action:(id)sender {
    
    self.res1();
    
}
- (IBAction)btn2Action:(id)sender {
    
    self.res2();

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
