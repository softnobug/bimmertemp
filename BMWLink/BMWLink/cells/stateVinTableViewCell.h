//
//  stateVinTableViewCell.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/17.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface stateVinTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UILabel *contentLa;

@end
