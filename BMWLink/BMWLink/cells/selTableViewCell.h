//
//  selTableViewCell.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//


typedef void(^selBtnBlock)(BOOL issel);
#import <UIKit/UIKit.h>

@interface selTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selBtn;

@property(nonatomic,copy)selBtnBlock mblck;
@property(nonatomic,assign)BOOL isSel;
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toLeft;
-(void)setSelState:(BOOL)hasSel;

@end
