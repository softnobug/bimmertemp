//
//  subSelTableViewCell.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/27.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subSelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UIButton *selBtn;
@property(nonatomic,copy)selBtnBlock mblck;
@property(nonatomic,assign)BOOL isSel;
-(void)setSelState:(BOOL)hasSel;

@end
