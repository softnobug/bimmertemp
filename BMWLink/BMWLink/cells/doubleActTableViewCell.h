//
//  doubleActTableViewCell.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBButton.h"
@interface doubleActTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet IBButton *btn1;
@property (weak, nonatomic) IBOutlet IBButton *btn2;
@property (weak, nonatomic) IBOutlet UILabel *titleLa1;
@property (weak, nonatomic) IBOutlet UILabel *titleLa2;

@property(nonatomic,copy)resultBlock res1;
@property(nonatomic,copy)resultBlock res2;

@end
