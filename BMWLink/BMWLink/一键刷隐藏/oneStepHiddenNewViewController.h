//
//  oneStepHiddenNewViewController.h
//  BMWLink
//
//  Created by weixue meng on 2018/10/10.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface oneStepHiddenNewViewController : UIViewController
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@end

NS_ASSUME_NONNULL_END
