//
//  oneStepHideLeftNumTableViewCell.h
//  BMWLink
//
//  Created by weixue meng on 2018/10/10.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface oneStepHideLeftNumTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLa;
@property (weak, nonatomic) IBOutlet UILabel *leftNumLa;

@end

NS_ASSUME_NONNULL_END
