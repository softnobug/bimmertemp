//
//  oneStepHiddenViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "oneStepHiddenViewController.h"

@interface oneStepHiddenViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *carvinLa;
@property (nonatomic, strong) NSIndexPath *selIndex;
@property (nonatomic, strong) NSMutableArray *contentArr;
@property (nonatomic, strong) NSMutableArray *numArr;

@property (nonatomic, strong) NSMutableArray *selContentArr;

@end

@implementation oneStepHiddenViewController

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"当前一键刷隐藏仅支持G平台新款车型!其他车型请勿使用此功能!" preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction *_Nonnull action) {
//    }];
//
//    [alert addAction:action1];
//    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initTableveiw];
//    [self readEsys];
    self.selContentArr = @[].mutableCopy;
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carvinLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@", lastVin, self.carPlat];

    self.contentArr = @[@"激活RDC实时胎压监测",
                        @"激活限速显示",
                        @"激活行车时看视频",
                        @"取消开机时显示确认界面",
                        @"开锁解锁声音提示",
                        @"M运动开机界面动画",
                        @"自动启停记忆",
                        @"空调记忆"].mutableCopy;

    self.numArr = @[@"10",
                        @"299",
                        @"10",
                        @"10",
                        @"10",
                        @"10",
                        @"10",
                        @"10"].mutableCopy;
}

- (void)readEsys
{
    NSDictionary *dict = [linkManager readTxtFile:@"Esysone.txt"];
    NSLog(@"%@", dict);
    self.contentArr = @[].mutableCopy;

    for (NSString *keyStr in [dict allKeys]) {
        NSMutableArray *tempArr = @[].mutableCopy;
        NSString *valueStr = [dict objectForKey:keyStr];
        [tempArr addObject:keyStr];
        NSLog(@"%@", valueStr);

        if (valueStr.length != 0) {
            NSArray *vArr = [valueStr componentsSeparatedByString:@","];
            [tempArr addObjectsFromArray:vArr];
        }

        [self.contentArr addObject:tempArr];
    }

    NSLog(@"contentArr:%@", self.contentArr);

    [self.tableview reloadData];
}

- (void)initTableveiw
{
    self.tableview.delegate = self;
    self.tableview.dataSource = self;

    [self.tableview registerNib:[UINib nibWithNibName:@"selTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];

    [self.tableview registerNib:[UINib nibWithNibName:@"subSelTableViewCell" bundle:nil] forCellReuseIdentifier:@"subcell"];

    [self.tableview registerNib:[UINib nibWithNibName:@"oneStepHideLeftNumTableViewCell" bundle:nil] forCellReuseIdentifier:@"one"];

    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;

    if (@available(iOS 11.0,*)) {
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carvinLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@", lastVin, self.carPlat];
    self.selIndex = [NSIndexPath indexPathForRow:0 inSection:-1];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.contentArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [self.contentArr[section]count];

//    if (self.selIndex.section == section) {
//        return [self.contentArr[section]count];
//    }

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row!=0 ) {
//
//        subSelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subcell"];
//        cell.titleLa.text = self.contentArr[indexPath.section][indexPath.row];
//        __weak typeof(cell) weakcell = cell;
//        cell.mblck = ^(BOOL issel) {
//
//            [self selIndex:indexPath];
//            [self hasSel:indexPath];
//            [weakcell setIsSel:[self hasSel:indexPath]];
//
//        };
//
//        return cell;
//
//    }

    oneStepHideLeftNumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"one"];
    
    cell.nameLa.text = self.contentArr[indexPath.section];
    cell.leftNumLa.text = self.numArr[indexPath.section];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;

//    selTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    __weak typeof(cell) weakcell = cell;
//
////    if ([self.contentArr[indexPath.section]count]!=1) {
////        cell.selBtn.hidden = YES;
////
////    }else{
//        cell.selBtn.hidden = NO;
////    }
//
//    cell.mblck = ^(BOOL issel) {
//
//        [self selIndex:indexPath];
//        [self hasSel:indexPath];
//        [weakcell setSelState:[self hasSel:indexPath]];
//
//    };
//
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//    return cell;
}

- (BOOL)hasSel:(NSIndexPath *)indexP
{
    for (NSIndexPath *indexN in self.selContentArr) {
        if (indexP.section == indexN.section) {
            return YES;
        }
    }

    return NO;
}

- (void)selIndex:(NSIndexPath *)indexm
{
    if (indexm.row != 0) {
        NSArray *ta = self.selContentArr;

        BOOL mhas = false;
        NSIndexPath *tmin;
        for (NSIndexPath *min in ta) {
            if (min.section == indexm.section) {
                mhas = YES;
                tmin = min;
            }
        }

        if (!mhas) {
            [self.selContentArr addObject:indexm];
        } else {
            [self.selContentArr removeObject:tmin];
            [self.selContentArr addObject:indexm];
        }
    } else {
        if ([self hasSel:indexm]) {
            [self.selContentArr removeObject:indexm];
        } else {
            [self.selContentArr addObject:indexm];
        }
    }

    NSLog(@"%ld", self.selContentArr.count);
    [self.tableview reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return .001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return .001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (self.selIndex == indexPath) {
//        self.selIndex = [NSIndexPath indexPathForRow:0 inSection:-1];
//    }else{
//        self.selIndex = indexPath;
//    }

    [tableView reloadData];
}

- (IBAction)backToRoot:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
