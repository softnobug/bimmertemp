//
//  oneStepHiddenNewViewController.m
//  BMWLink
//
//  Created by weixue meng on 2018/10/10.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import "oneStepHiddenNewViewController.h"

@interface oneStepHiddenNewViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *vinLa;

@end

@implementation oneStepHiddenNewViewController


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提醒" message:@"当前一键刷隐藏仅支持G平台新款车型!其他车型请勿使用此功能!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
    
//    {
//        "激活限速显示":"",
//        "激活RDC实时胎压监测":"",
//        "激活行车时看视频":"",
//        "取消开机时显示确认界面":"",
//        "开锁解锁声音提示":"",
//        "M运动开机界面动画":"",
//        "自动启停记忆":"",
//        "空调记忆":""
//    }
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
