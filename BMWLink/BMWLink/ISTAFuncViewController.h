//
//  ISTAFuncViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISTAFuncViewController : UIViewController
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
@property(nonatomic,strong)NSString *keyStr;
+(void)hideP;
+(void)showP;
@end
