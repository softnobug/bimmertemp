//
//  bluetoothManager.m
//  sassetup
//
//  Created by weixuemeng on 2018/4/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "bluetoothManager.h"
static NSString * const kServiceUUID = @"312700E2-E798-4D5C-8DCF-49908332DF9F";
static NSString * const kCharacteristicUUID = @"FFA28CDE-6525-4489-801C-1C060CAC9767";
@implementation bluetoothManager
static bluetoothManager *blueManager;
-(void)initObects{
    
    self.peripheralArray=[NSMutableArray array];
    self.recvData=[NSMutableData data];
    //中心
    self.cbCentralMgr=[[CBCentralManager alloc]initWithDelegate:self queue:nil];
    self.cbCentralMgr.delegate =self;
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
    //周边
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
    self.peripheral.delegate=self;
    self.myValue = @"";
    //搜索蓝牙
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self scanBluetooth];
        
    });
    
}

/*----------------蓝牙-----------------------*/

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    
    [self scanBluetooth];
    
    NSLog(@"当前状态%ld",central.state);
    switch (central.state) {
        case CBCentralManagerStateUnknown:
            //            NSLog(@">>>CBCentralManagerStateUnknown");
            break;
            
        case CBCentralManagerStateResetting:
            //            NSLog(@">>>CBCentralManagerStateResetting");
            
            break;
        case CBCentralManagerStateUnsupported:
            //            NSLog(@">>>CBCentralManagerStateUnsupported");
            break;
            
        case CBCentralManagerStateUnauthorized:
            //            NSLog(@">>>CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStatePoweredOff:
            //            NSLog(@">>>CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStatePoweredOn:
            //            NSLog(@">>>CBCentralManagerStatePoweredOn");
            
            
            break;
        default:
            break;
    }
    
}


#pragma mark 发现一个Peripherals

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
//    NSLog(@"Peripherals:%@,RSSI=%@",[peripheral.name isEqual:[NSNull null]]?@"":peripheral.name,RSSI);
    
    if(![_peripheralArray containsObject:peripheral]){
        
//        CarLync_GW  IOS-Vlink
        if (peripheral.name != nil&&[peripheral.name isEqualToString:@"IOS-Vlink"]) {
            
            NSLog(@"Peripherals:%@,RSSI=%@",[peripheral.name isEqual:[NSNull null]]?@"":peripheral.name,RSSI);
            
            [_peripheralArray addObject:peripheral];
            
            self.disBlock(_peripheralArray.mutableCopy);
            
        }
        
    }
    
}


#pragma mark 连接成功

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    
    self.hasAuth = YES;
    self.hasConnected = YES;
    self.conBlock(1);
    
//    self.hud.hidden=YES;
//
//
//    MBProgressHUD  *myhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    myhud.mode = MBProgressHUDModeIndeterminate;
//    myhud.labelText = @"正在连接";
//    [myhud hide:YES afterDelay:1];
    
    NSLog(@">>>连接到名称为（%@）的设备-成功",peripheral.name);
    
    [peripheral setDelegate:self];
    [peripheral discoverServices:nil];
    
    //    //准备特征和服务
    [self setupService];
//    status.text = @"已连接";
    
}


//连接到Peripherals-失败
-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error

{
    
    NSLog(@"连接失败:%@",error);
    self.hasConnected = NO;

    self.conBlock(0);
    
    [self.cbCentralMgr connectPeripheral:self.peripheral options:nil];

}


#pragma mark Peripherals断开连接

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    
    self.conBlock(3);
    self.hasConnected = NO;

//    self.hud.hidden=YES;
    
    
    NSLog(@">>>外设连接断开连接 %@: %@\n", [peripheral name], [error localizedDescription]);
    
//    status.text = @"连接中";
    
    NSString *lan = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    [self.cbCentralMgr connectPeripheral:self.peripheral options:nil];
    
}


#pragma mark ===========================周边代理========================================

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn:
            //            [self setupService];
            break;
            
        default:
            
            NSLog(@"Peripheral Manager did change state");
            
            break;
    }
    
    
}

//准备服务和特征

-(void)setupService{
    
    NSLog(@"========准备服务和特征=========");
    
    CBUUID *characteristicUUID = [CBUUID UUIDWithString:kCharacteristicUUID];
    
    _customCharacteristic = [[CBMutableCharacteristic alloc] initWithType:characteristicUUID properties:CBCharacteristicPropertyNotify | CBCharacteristicPropertyRead | CBCharacteristicPropertyWrite | CBCharacteristicPropertyWriteWithoutResponse value:nil permissions:CBAttributePermissionsReadable|CBAttributePermissionsWriteable];
    
    CBUUID *serviceUUID = [CBUUID UUIDWithString:kServiceUUID];
    
    _customService = [[CBMutableService alloc] initWithType:serviceUUID primary:YES];
    
    //将特征添加到了服务上
    
    [_customService setCharacteristics:@[_customCharacteristic]];
    
    //把服务添加到周边管理者（Peripheral Manager）是用于发布服务
    
    //周边管理者会通知他的代理方法-peripheralManager:didAddService:error:
    
    [_peripheralManager addService:_customService];
    
    NSLog(@"========Adding Service!!");
    
}


//发现服务

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    
    
    NSLog(@"========发现服务=========");
    
    for (CBService *service in peripheral.services)
    {
        //        NSLog(@"serviceCount=%ld",service.includedServices.count);
        
        NSLog(@"uuid===%@",service.UUID);
        
        //        if ([service.UUID isEqual:[CBUUID UUIDWithString:@"FFE0"]]) {
        
        [peripheral discoverCharacteristics:nil forService:service];
        
        //        }
        
        
    }
    
    
}


//添加服务

- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error
{
    
    
    NSLog(@"=============添加服务==================");
    
    if (error == nil) {
        // Starts advertising the service
        
        [self.peripheralManager startAdvertising:
         @{ CBAdvertisementDataLocalNameKey :
                @"ICServer", CBAdvertisementDataServiceUUIDsKey :
                @[[CBUUID UUIDWithString:kServiceUUID]] }];
        
        
    }
    
}

//扫描到Characteristics

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
//    NSLog(@"扫描到Characteristics");
    
    for (CBCharacteristic *characteristic in service.characteristics)
        
    {
        
        self.characteristic=characteristic;
        
        [_peripheral setNotifyValue:YES forCharacteristic:_characteristic];
        
//        NSLog(@"service:%@ 的 Characteristic: %@",service.UUID,characteristic.UUID);
        
        
    }
    
    //获取Characteristic的值，读到数据会进入方法：-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
    
    for (CBCharacteristic *characteristic in service.characteristics){
        {
            
            [peripheral readValueForCharacteristic:characteristic];
            
        }
    }
    
    
    //搜索Characteristic的Descriptors，读到数据会进入方法：-(void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
    for (CBCharacteristic *characteristic in service.characteristics){
        
        [peripheral discoverDescriptorsForCharacteristic:characteristic];
        
    }
    
}


#pragma mark 收到蓝牙的值

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if (!self.hasAuth) {
        
        return;
        
    }
    
    [self.recvData appendData:characteristic.value];

    
    NSString * str1  =[[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    
//    NSLog(@"====1====str值%@",str1);
    
    
//    NSString * str  =[[NSString alloc] initWithData:self.recvData encoding:NSUTF8StringEncoding];
//
//    NSLog(@"====2====str值%@",str);
    
    
    if ([str1 hasSuffix:@">"]) {
        
        _myValue = [NSString stringWithFormat:@"%@%@",_myValue,str1];
//        NSLog(@"========_myValue值%@",_myValue);
        self.recBlock(_myValue);
        _recvData = nil;
        _myValue = @"";
        
    }else{
        
        _myValue = [NSString stringWithFormat:@"%@%@",_myValue,str1];
//        NSLog(@"=====_myValue:%@",_myValue);
        
    }
    
    
}

//将传入的NSData类型转换成NSString并返回
- (NSString*)hexadecimalString:(NSData *)data{
    NSString* result;
    const unsigned char* dataBuffer = (const unsigned char*)[data bytes];
    if(!dataBuffer){
        return nil;
    }
    NSUInteger dataLength = [data length];
    NSMutableString* hexString = [NSMutableString stringWithCapacity:(dataLength * 2)];
    for(int i = 0; i < dataLength; i++){
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    result = [NSString stringWithString:hexString];
    return result;
}
//将传入的NSString类型转换成NSData并返回
- (NSData*)dataWithHexstring:(NSString *)hexstring{
    NSData* aData;
    return aData = [hexstring dataUsingEncoding: NSASCIIStringEncoding];
}

//搜索到Characteristic的Descriptors
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    //打印出Characteristic和他的Descriptors
    NSLog(@"characteristic uuid:%@",characteristic.UUID);
    
    for (CBDescriptor *d in characteristic.descriptors) {
        
        NSLog(@"Descriptor uuid:%@",d.UUID);
        
    }
    
    
    
}

//获取到Descriptors的值
-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error{
    
    //打印出DescriptorsUUID 和value
    //这个descriptor都是对于characteristic的描述，一般都是字符串，所以这里我们转换成字符串去解析
    
    NSLog(@"characteristic uuid:%@  value:%@",[NSString stringWithFormat:@"%@",descriptor.UUID],descriptor.value);
    
    
}


//设置通知
-(void)notifyCharacteristic:(CBPeripheral *)peripheral
             characteristic:(CBCharacteristic *)characteristic{
    
    //设置通知，数据通知会进入：didUpdateValueForCharacteristic方法
    [peripheral setNotifyValue:YES forCharacteristic:characteristic];
    
    
}


//取消通知
-(void)cancelNotifyCharacteristic:(CBPeripheral *)peripheral
                   characteristic:(CBCharacteristic *)characteristic{
    
    [peripheral setNotifyValue:NO forCharacteristic:characteristic];
    
    
}

-(void)scanBluetooth{
    
//    if (!self.hasAuth) {
//        return;
//    }
    NSLog(@"扫描");
    
    NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:false],CBCentralManagerScanOptionAllowDuplicatesKey,nil];
    
    [_cbCentralMgr scanForPeripheralsWithServices:nil options:dic];
    
}
//断开连接
-(void)cancelConect{
    [_cbCentralMgr cancelPeripheralConnection:self.peripheral];
}
#pragma mark ================中心==================

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
}


-(void)connectAction:(CBPeripheral * )peripheral{
    
    [self.cbCentralMgr connectPeripheral:peripheral options:nil];
    
}

-(void)sendMessage:(NSString *)message{
    
    if (!self.hasAuth) {
        return;
    }
    
    message = [bluetoothManager hexStringFromString:message];
    
//    NSLog(@"发值:%@",message);
    NSData *sendData=[self hexToBytes:message];
//    [ _peripheral writeValue:sendData forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
    
//    NSData *sendData = [self HexConvertToASCII:message];
    
    if (_characteristic==nil) {
        return;
    }
    [ _peripheral writeValue:sendData forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
    
}

+ (NSString *)hexStringFromString:(NSString *)string{
    NSData *myD = [string dataUsingEncoding:NSUTF8StringEncoding];
    Byte *bytes = (Byte *)[myD bytes];
    //下面是Byte 转换为16进制。
    NSString *hexStr=@"";
    for(int i=0;i<[myD length];i++)
    {
        NSString *newHexStr = [NSString stringWithFormat:@"%x",bytes[i]&0xff];///16进制数
        if([newHexStr length]==1)
            hexStr = [NSString stringWithFormat:@"%@0%@",hexStr,newHexStr];
        else
            hexStr = [NSString stringWithFormat:@"%@%@",hexStr,newHexStr];
    }
    return hexStr;
}

-(NSData *)HexConvertToASCII:(NSString *)hexString{
    int j=0;
    Byte bytes[22];  ///3ds key的Byte 数组， 128位
    for(int i=0;i<[hexString length];i++)
    {
        int int_ch;  /// 两位16进制数转化后的10进制数
        
        unichar hex_char1 = [hexString characterAtIndex:i]; ////两位16进制数中的第一位(高位*16)
        
        int int_ch1;
        
        if(hex_char1 >= '0' && hex_char1 <='9')
            
            int_ch1 = (hex_char1-48)*16;   //// 0 的Ascll - 48
        
        else if(hex_char1 >= 'A' && hex_char1 <='F')
            
            int_ch1 = (hex_char1-55)*16; //// A 的Ascll - 65
        
        else
            
            int_ch1 = (hex_char1-87)*16; //// a 的Ascll - 97
        
        i++;
        
        unichar hex_char2 = [hexString characterAtIndex:i]; ///两位16进制数中的第二位(低位)
        
        int int_ch2;
        
        if(hex_char2 >= '0' && hex_char2 <='9')
            
            int_ch2 = (hex_char2-48); //// 0 的Ascll - 48
        
        else if(hex_char1 >= 'A' && hex_char1 <='F')
            
            int_ch2 = hex_char2-55; //// A 的Ascll - 65
        
        else
            
            int_ch2 = hex_char2-87; //// a 的Ascll - 97
        
        
        
        int_ch = int_ch1+int_ch2;
        
        NSLog(@"int_ch=%d",int_ch);
        
        bytes[j] = int_ch;  ///将转化后的数放入Byte数组里
        
        j++;
        
    }
    
    NSData *newData = [[NSData alloc] initWithBytes:bytes length:22];
    return newData;
}

-(NSData*) hexToBytes:(NSString *)stringN {
    
    NSMutableData* data = [NSMutableData data];
    
    int idx;
    
    for (idx = 0; idx+2 <= stringN.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [stringN substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
        
    }
    
    return data;
    
}

+(bluetoothManager*)shareInstance{
    
    if (!blueManager) {
        blueManager = [[bluetoothManager alloc]init];
    }
    return blueManager;
}


@end
