//
//  StringPure.m
//  BMWLink
//
//  Created by weixue meng on 2018/9/13.
//  Copyright © 2018 com.softnobug. All rights reserved.
//

#import "StringPure.h"

@implementation StringPure

//去除字符串中的空格及其他发号
+(NSString*)stringPure:(NSString*)orginStr{
    
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~$€^•'@#$%^&*()_+'\""];
//    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];

    orginStr = [orginStr stringByTrimmingCharactersInSet:set];
    NSCharacterSet *whitespace = [NSCharacterSet  whitespaceAndNewlineCharacterSet];
    orginStr = [orginStr stringByTrimmingCharactersInSet:whitespace];
    return orginStr;
    
}
@end
