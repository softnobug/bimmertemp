//
//  bluetoothlistVeiw.m
//  airdetector
//
//  Created by 孟维学 on 2016/12/29.
//  Copyright © 2016年 carlync. All rights reserved.
//

#import "bluetoothlistVeiw.h"
#import <CoreBluetooth/CoreBluetooth.h>

@implementation bluetoothlistVeiw

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setupview];
    
    }
    return self;
}


-(void)setupview{

    self.backView = [[UIView alloc]initWithFrame:self.bounds];
    self.backView.backgroundColor =[UIColor blackColor];
    self.backView.alpha = .7;
    
    [self addSubview:self.backView];
    
    self.tableview = [[UITableView alloc]initWithFrame:CGRectMake(20, 0, self.frame.size.width-40, 200) style:UITableViewStyleGrouped];
    self.tableview.center = self.center;
    self.tableview.delegate = self;
    self.tableview.dataSource =self;
    [self addSubview:self.tableview];
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableview.layer setMasksToBounds:YES];
    self.tableview.layer.cornerRadius = 10;
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(20, CGRectGetMaxY(self.tableview.frame)+20, self.frame.size.width-40, 40);
    [button setTitle:@"取消" forState:UIControlStateNormal];

    NSString *lan = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
        if ([lan isEqualToString:@"de"]) {
            [button setTitle:@"取消" forState:UIControlStateNormal];
            [button setTitle:@"Cancel" forState:UIControlStateNormal];
            
        }
    
        if ([lan isEqualToString:@"english"]) {
            [button setTitle:@"Cancel" forState:UIControlStateNormal];

        }
    
        if ([lan isEqualToString:@"han"]) {
            [button setTitle:@"取消" forState:UIControlStateNormal];

        }
    
    [button setBackgroundColor:[UIColor whiteColor]];
    [button.layer setMasksToBounds:YES];
    button.layer.cornerRadius = 5;
    [self addSubview:button];
    [button setTitleColor:[UIColor colorWithHexString:@"101010"] forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
}

//取消
-(void)cancelAction{
    
    [self removeFromSuperview];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listArr.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 40;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    CBPeripheral * peripheral = self.listArr[indexPath.row];
    cell.textLabel.text = peripheral.name;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CBPeripheral * peripheral = self.listArr[indexPath.row];
    
    NSLog(@"%@",peripheral.name);
    
    self.blueSel(peripheral);
    
    [self removeFromSuperview];
    
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
