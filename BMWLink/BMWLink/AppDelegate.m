//
//  AppDelegate.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@property (nonatomic, strong) AFHTTPRequestOperation *operation1;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[bluetoothManager shareInstance]initObects];
    
    
//    [smartAFN smartAFNGet:@"http://www.car-lync.com/myorder/vin.txt" parameters:nil success:^(NSDictionary *dict) {
//
//        NSLog(@"dict========%@",dict);
//
//    } failure:^(NSError *error) {
//
//
//    }];
    
    

    
//    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
//    self.window.backgroundColor = [UIColor whiteColor];
//    testViewController *test = [[testViewController alloc]initWithNibName:@"testViewController" bundle:nil];
//    self.window.rootViewController = test;
//    [self.window makeKeyAndVisible];
    
//    /var/mobile/Containers/Data/Application/7BC1D6CC-DC03-4BE7-A758-F363AFF2D480/Documents/
//    BTLD_000011A7_005_001_001_2018_09_13_10_06.txt
//    NSArray *arr =  [FileManager readTxt:@"BTLD_000011A7_005_001_001.txt"];
//    NSLog(@"==%@",arr);
    
    
//    self.nowDownload = YES;
    
//    NSArray *m = [FileManager myQuickMethod:@"cafdraw"];
//    NSLog(@"cafdraw:%@",m);
    
//    [FileManager readXML:@"CAFD_000017BC_103_041_030"];
//
//    [smartAFN downLoadWithName:@"CAFD_000000D0_003_004_004" success:^{
//
//    } fail:^{
//
//    }];
    
    
    
    
//    NSString *m =[[NSUserDefaults standardUserDefaults]objectForKey:@"faf"];
//
//    NSLog(@"%@",[self cafdArr:m]);
    
//    NSArray *arr = [FileManager myQuickMethod:@"ncd"];
//    NSLog(@"ncd:%@",arr);
    
//    [self down];
    
//    [smartAFN downLoadWithFileName:@"q.txt" success:^{
//
//
//    } fail:^{
//
//    }];
    
    return YES;
    
}

//-(void)down{
//
//    AFHTTPRequestOperation *operation1;
//
//    NSString * urlStr = @"http://www.car-lync.com/myorder/";
//
//    // 任务1
//    operation1 = [LCDownloadManager downloadFileWithURLString:urlStr cachePath:@"vin.txt" progress:^(CGFloat progress, CGFloat totalMBRead, CGFloat totalMBExpectedToRead) {
//
//        NSLog(@"=====progress: %.2f ", progress);
//
//
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//
//        NSLog(@"Task1 -> Download finish");
//
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//
//
//        if (error.code == -999) NSLog(@"Task1 -> Maybe you pause download.");
//
//        NSLog(@"Task1 -> %@", error);
//
//    }];
//
//}


-(NSArray*)cafdArr:(NSString*)mstr{
    
    NSMutableArray *titleArr = @[].mutableCopy;
    //    mstr = [StringPure stringPure:mstr];
    for (int i = 0; i<mstr.length/26; i++) {
        [titleArr addObject:[mstr substringWithRange:NSMakeRange(i*26+1, 26-1)]];
    }
    return titleArr.mutableCopy;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
