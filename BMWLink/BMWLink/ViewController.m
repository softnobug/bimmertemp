//
//  ViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "ViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>


@interface ViewController ()<GCDAsyncSocketDelegate,UIPickerViewDelegate>
{
    bluetoothlistVeiw *listview;
}
// 客户端GCDAsyncSocket
@property(nonatomic,strong)GCDAsyncSocket*clientSocket;
@property(nonatomic,assign)BOOL connected;
@property(nonatomic,assign)BOOL firstConnect;
@property(nonatomic,strong)NSTimer*connectTimer;
@property(nonatomic,assign)NSInteger sheep;
@property(nonatomic,strong)NSMutableArray *sheepContent;
@property (weak, nonatomic) IBOutlet UIPickerView *picketView;
@property(nonatomic,strong)NSArray *contentArr;
@property (weak, nonatomic) IBOutlet UILabel *platForm;
@property (weak, nonatomic) IBOutlet UILabel *carForm;
@property (weak, nonatomic) IBOutlet UILabel *cplat;
@property (weak, nonatomic) IBOutlet UILabel *carplat;
@property (weak, nonatomic) IBOutlet UILabel *deslabel;
@property (weak, nonatomic) IBOutlet IBButton *connectBtn;
@property (weak, nonatomic) IBOutlet UILabel *conningLa;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progreV;
@property (weak, nonatomic) IBOutlet UILabel *backLine;
@property (weak, nonatomic) IBOutlet UILabel *frontLine;
@property (weak, nonatomic) IBOutlet UIButton *settintBtn;
@property(nonatomic,strong)NSString*vimCarPlat;
@property(nonatomic,strong)NSString *curFileName;
@property(nonatomic,strong)NSDictionary *asdict;
@property(nonatomic,strong)NSTimer *ftimer;

@property(nonatomic,strong)NSMutableArray *peripheralArray;


@end

@implementation ViewController
static doneBlock mstartB;
static doneBlock mstopB;
static receMesBlock recBlock;
static NSArray *sheepArr;
static GCDAsyncSocket*clientSocket;
static NSInteger sheep;

-(void)viewWillAppear:(BOOL)animated{

    
    self.navigationController.navigationBarHidden = YES;
    self.progreV.hidesWhenStopped = YES;
    
//    [self.progreV startAnimating];
    
//    NSLog(@"%@",[[linkManager tValueToArr:@"640t.txt"][0]objectForKey:@"640F110"]);
    
//    NSLog(@"U640U:%@",[linkManager arrContainArr:@"ISTA_check.txt"]);
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _sheep = 0;
    
    self.firstConnect = YES;
    self.clientSocket = [[GCDAsyncSocket alloc]initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    self.sheepContent = @[].mutableCopy;
    
    [self.settintBtn setImage:[UIImage imageNamed:@"setupdown.png"] forState:UIControlStateNormal];
    
    [self.settintBtn setImage:[UIImage imageNamed:@"setupup.png"] forState:UIControlStateSelected];
    
    self.picketView.delegate = self;
    
    self.contentArr = @[
                        @[@"F001",@"F平台",@"7系、6系、5系GT"],
                        @[@"F010",@"F平台",@"5系"],
                        @[@"F020",@"F平台",@"1系、2系、3系、3系GT、4系"],
                        @[@"F025",@"F平台",@"x3、x4、x5、x6"],
                        @[@"F056",@"F平台",@"Mini全系、新款x1"],
                        @[@"S15",@"G平台",@"新款7系、新款5系"],
                        @[@"I001",@"电动",@"i3、i8"]
                        ];
    
    [self.picketView selectRow:2 inComponent:0 animated:YES];
    self.vimCarPlat = self.contentArr[2][0];
    self.platForm.text =self.contentArr[2][1];
    self.carForm.text =self.contentArr[2][2];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cleanAction) name:@"clean" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(istaFuncAction:) name:@"ISTA" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(istaExtFuncAction:) name:@"istaEXt" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(istaExtStopFuncAction:) name:@"istaEXtStop" object:nil];

    //升级程序写入变速箱复位
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bsxWrite:) name:@"bsxwrite" object:nil];
    
    //读取当前车辆数据
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(bsxRead:) name:@"bsxread" object:nil];
    
    //CAFD
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cafdRead:) name:@"cafdFirst" object:nil];

//    CAFDsend

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cafdSend:) name:@"cafdsend" object:nil];
    
    [self showBluetoothList];
    
    [bluetoothManager shareInstance].disBlock = ^(NSArray *blueArr) {
        
        listview.listArr = blueArr;
        [listview.tableview reloadData];
        
    };
    //0-成功  1-失败  2-断开连接
    [bluetoothManager shareInstance].conBlock = ^(NSInteger m) {
        
//        NSLog(@"连接状态:%ld",m);
        
        [bluetoothManager shareInstance].recBlock = ^(NSString *message) {
            
            NSLog(@"收到的~值:%@",message);
            if ([message containsString:@">"]){
                
                [self receiveB:message];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [self sendSheep:self.curFileName];

                });
            }
            
        };
    };
    
}

-(void)cafdSend:(NSNotification*)notify{
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"cafdsend.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
}


-(void)showBluetoothList{
    
    listview = [[bluetoothlistVeiw alloc]initWithFrame:self.view.frame];
    _peripheralArray = [bluetoothManager shareInstance].peripheralArray;
    __weak typeof (self) weakself = self;
    listview.blueSel = ^(CBPeripheral * peripheral){
        
//        [weakself recevieAction];
        
        [bluetoothManager shareInstance].peripheral=peripheral;
        [bluetoothManager shareInstance].peripheral.delegate = self;
        [[bluetoothManager shareInstance]connectAction:peripheral];
        
    };
    
    [[[[UIApplication sharedApplication]delegate]window]addSubview:listview];
    
    listview.listArr = _peripheralArray;
    
    [listview.tableview reloadData];
    
}


//读取当前车辆数据-变速箱
-(void)cafdRead:(NSNotification*)notify{
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"CAFD_00001EF6_006_020_007.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
}



+(void)sockerFunc:(NSString*)sendStr receive:(receMesBlock)receB start:(doneBlock)startBlock end:(doneBlock)endBlock{
    
//    sheep=0;
    startBlock();
//    sheepArr = sendMesArr;
    mstopB = endBlock;
    recBlock = receB;
//    [ViewController sendSheepArr];
    [ViewController sendData:sendStr];
    
}

+(void)sendSheepArr{
    
    [ViewController sendData:sheepArr[sheep]];
    sheep++;
    NSLog(@"新sheep:%ld",sheep);
    
}

+(void)sendData:(NSString*)message{
    
    NSLog(@"新当前发值:%@",message);
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    [clientSocket writeData:data withTimeout:- 1 tag:0];
    
}

//读取当前车辆数据-变速箱
-(void)bsxRead:(NSNotification*)notify{
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"bsxread.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
}


//升级程序写入变速箱复位
-(void)bsxWrite:(NSNotification*)notify{
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"bsxwrite.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
}

-(void)istaExtStopFuncAction:(NSNotification*)notify{
    
    self.curFileName = @"";
    NSLog(@"***********");
//    [self.ftimer invalidate];
//    self.ftimer = nil;
    
}

-(void)istaExtFuncAction:(NSNotification*)notify{
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"ISTA_contralext.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
}

//
-(void)istaFuncAction:(NSNotification*)notify{
    
    
    NSDictionary *dict = [notify userInfo];
    NSArray *arr = [dict objectForKey:@"ista"];
    self.curFileName = @"ISTA_contral.txt";
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    self.sheep = 0;
    [self sendSheep:self.curFileName];
    
    
}

#pragma mark 清楚故障码
-(void)cleanAction{
    
    self.sheep = 0;
    self.curFileName = @"clean.txt";
    [self readFile:self.curFileName];
    [self sendSheep:self.curFileName];
    
}




// 返回多少行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return self.contentArr.count;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
//    NSLog(@"row:%ld  component:%ld",row,component);
    
    self.platForm.text =self.contentArr[row][1];
    self.carForm.text =self.contentArr[row][2];
    self.vimCarPlat = self.contentArr[row][0];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    //设置分割线的颜色
    for(UIView *singleLine in pickerView.subviews)
    {
        if (singleLine.frame.size.height < 1)
        {
            singleLine.backgroundColor = [UIColor whiteColor];
        }
    }
    
    //设置文字的属性
    UILabel *genderLabel = [UILabel new];
    genderLabel.textAlignment = NSTextAlignmentCenter;
    genderLabel.text = self.contentArr[row][0];
    genderLabel.font = [UIFont systemFontOfSize:17];
    genderLabel.textColor = [UIColor whiteColor];
    
    return genderLabel;
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}


- (void)readFile:(NSString *)fileName
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSString *content = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    content = [content stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    NSArray *arr = [content componentsSeparatedByString:@","];
    [self.sheepContent removeAllObjects];
    [self.sheepContent addObjectsFromArray:arr];
    
    NSLog(@"读值成功:%ld  %@",self.sheepContent.count,self.sheepContent);
    
    
}


//连接
- (IBAction)connect:(id)sender {

//    NSError *error = nil;
//
//    self.connected = [self.clientSocket connectToHost:@"192.168.0.10" onPort:35000 viaInterface:nil withTimeout:-1 error:&error];
//
//    [self showOnProgress:YES];
    
//    self.sheep = 0;
//    self.curFileName = @"F001.txt";
//    [self readFile:self.curFileName];
//    [self sendSheep:self.curFileName];
    
    [self showBluetoothList];
    
    
}


//发送数据
- (IBAction)send:(id)sender {
    
    self.sheep = 0;
    self.curFileName = @"F001.txt";
    [self readFile:self.curFileName];
    [self sendSheep:self.curFileName];
    
}




-(void)sendSheep:(NSString *)fileName{
    
    if (self.sheepContent.count==0) {
        NSLog(@"ppp1");
        return;
    }
    
    if (self.sheep>self.sheepContent.count-1) {
        
        NSLog(@"eee1");
        return;
        
    }
    
//    [self sendData:[NSString stringWithFormat:@"%@",self.sheepContent[self.sheep]]];
    
    [self sendData:[NSString stringWithFormat:@"%@\n\r",self.sheepContent[self.sheep]]];
    
    self.sheep++;
    
    return;
    
    /*=============获取Vin识别码---等待控制==============*/
    if ([self.curFileName isEqualToString:@"VIN.txt"]) {
        if (self.sheep == 0) {
            NSLog(@"UUUU");
            dispatch_async(dispatch_get_main_queue(), ^{
//                [self showOnProgress:YES];
            });
            
        }else if (self.sheep == self.sheepContent.count-1){
            NSLog(@"tttt");
            dispatch_async(dispatch_get_main_queue(), ^{
//                [self showOnProgress:NO];
            });

        }
    }
    
    /*=============清楚故障码==============*/
    if ([fileName isEqualToString:@"clean.txt"]&&self.sheep==0) {
        [carVinNumViewController showP];
        
    }
    
    if ([fileName isEqualToString:@"clean.txt"]&&(self.sheep>self.sheepContent.count-1)){
        
        [carVinNumViewController hideP];
        
    }
    
    if (![fileName isEqualToString:@"VIN.txt"]&&self.sheep==0) {
        if (![fileName isEqualToString:@"ISTA_contral.txt"]) {
            
            if ([fileName isEqualToString:@"bsxread.txt"]||[fileName isEqualToString:@"bsxwrite.txt"]) {
                [bsxNextViewController showP];
            }else{
                [recVimViewController showP];
                [ISTAViewController showP];
            }
            
        }else{
            [ISTAFuncViewController showP];
        }
    }
    
    if (![fileName isEqualToString:@"VIN.txt"]&&(self.sheep>=self.sheepContent.count-1)){
        
        if (![fileName isEqualToString:@"ISTA_contral.txt"]) {
            
            if ([fileName isEqualToString:@"bsxread.txt"]||[fileName isEqualToString:@"bsxwrite.txt"]) {
                
                [bsxNextViewController hideP];
                
            }else{
                [recVimViewController hideP];
                [ISTAViewController hideP];
            }
            
        }else{
            
            [ISTAFuncViewController hideP];
        }
        
    }
    
    if (self.sheepContent.count==0) {
        NSLog(@"ppp");
        return;
    }
    
    if (self.sheep>self.sheepContent.count-1) {
        
        NSLog(@"eee");
        
        return;
    }
    
    NSLog(@"kkkkkkkkk");
    
//    [[bluetoothManager shareInstance]sendMessage:[NSString stringWithFormat:@"%@\r",self.sheepContent[self.sheep]]];
    
    [self sendData:[NSString stringWithFormat:@"%@\r",self.sheepContent[self.sheep]]];
    self.sheep++;

}


-(void)sendData:(NSString*)message{
    
//    NSLog(@"当前发值:%@",message);
    
//    message = [ViewController stringFromHexString:message];
    
//    NSData *data1 = [message dataUsingEncoding:NSUTF8StringEncoding];
////    NSLog(@"当前发值data:%@",data1);
//    message = [NSString stringWithFormat:@"%@",data1];
//    message = [message stringByReplacingOccurrencesOfString:@"\r" withString:@""];
//    message = [message stringByReplacingOccurrencesOfString:@"\t" withString:@""];
//    message = [message stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    message = [message stringByReplacingOccurrencesOfString:@">" withString:@""];
//    message = [message stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"%ld发值:%@",self.sheep,message);
    [[bluetoothManager shareInstance]sendMessage:message];
    
//    return;
//    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
//    [self.clientSocket writeData:data withTimeout:- 1 tag:0];
    
    
}

#pragma mark  读取服务端数据
//- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
//{
//
//    NSString *text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//    NSLog(@"%ld收值:%@",self.sheep,text);
//
//    if ([self.curFileName isEqualToString:@"cafdsend.txt"]) {
//
//    }
//
////    CAFD
//    if ([self.curFileName isEqualToString:@"CAFD_00001EF6_006_020_007.txt"]) {
//
//        if ([text containsString:@"663F1"]) {
//            [CAFDViewController receIveToCreatData:text];
//        }
//
//    }
//
//    if (recBlock) {
//        recBlock(text);
//    }
//
//    if ([text containsString:@">"]){
//        if (mstopB) {
//            mstopB();
//        }
//    }
//
//    if ([text containsString:@"CAN ERROR"]) {
//        [self alertTitle:@"连接错误" content:@"请检查您的适配器已正确插入车辆的OBD接口,且手机已通过WiFi或蓝牙连接到适配器."];
//    }
//
//    if ([self.curFileName isEqualToString:@"VIN.txt"]) {
//        [self showVinNum:text];
//    }
//
//    if ([self.curFileName containsString:self.vimCarPlat]) {
//
//        [self receStateASD:text];
//
//    }
//
//    if ([self.curFileName isEqualToString:@"ISID_send.txt"]) {
//        [self receStateISID:text];
//    }
//
//
//
//    [self receiveWiFiData:text];
//    [self F001Module:text];
//    // 读取到服务端数据值后,能再次读取
//    [self.clientSocket readDataWithTimeout:- 1 tag:0];
//
//}

-(void)receiveB:(NSString*)text{
    
    if ([self.curFileName isEqualToString:@"cafdsend.txt"]) {
        
    }
    
    NSLog(@"%ld收值:%@",self.sheep,text);
    
    //    CAFD
    if ([self.curFileName isEqualToString:@"CAFD_00001EF6_006_020_007.txt"]) {
        
        if ([text containsString:@"663F1"]) {
            [CAFDViewController receIveToCreatData:text];
        }
        
    }
    
    if (recBlock) {
        recBlock(text);
    }
    
    if ([text containsString:@">"]){
        if (mstopB) {
            mstopB();
        }
    }
    
//    if ([text containsString:@"CAN ERROR"]) {
//        [self alertTitle:@"连接错误" content:@"请检查您的适配器已正确插入车辆的OBD接口,且手机已通过WiFi或蓝牙连接到适配器."];
//    }
    
    if ([self.curFileName isEqualToString:@"VIN.txt"]) {
        [self showVinNum:text];
    }
    
    if ([self.curFileName containsString:self.vimCarPlat]) {
        
        [self receStateASD:text];
        
    }
    
    if ([self.curFileName isEqualToString:@"ISID_send.txt"]) {
        [self receStateISID:text];
    }
    
    [self receiveWiFiData:text];
    [self F001Module:text];
    
}


-(void)receStateASD:(NSString *)rText{
    
    if (rText.length<3) {
        return;
    }
    
//    NSLog(@"ASD_sub:%@",[rText substringWithRange:NSMakeRange(0, 3)]);
    
    if ([[self.asdict allKeys]containsObject:[rText substringWithRange:NSMakeRange(0, 3)]]) {
        
        [recVimViewController addDataStr:rText];
        
    }
    
}

-(void)receStateISID:(NSString *)rText{
    
//    NSLog(@"ISID_sub:%@",[rText substringWithRange:NSMakeRange(0, 3)]);
    
    if (rText.length<3) {
        return;
    }
    if ([[self.asdict allKeys]containsObject:[rText substringWithRange:NSMakeRange(0, 3)]]) {
        
        NSLog(@"isid加:%@",rText);
        [ISTAViewController addDataStr:rText];
        
    }
    
}

#pragma mark  收值处理
-(void)receiveWiFiData:(NSString *)recStr{
    
    if (self.sheep>self.sheepContent.count-1) {
        
        NSLog(@"kkkk");
        
        if ([self.curFileName isEqualToString:@"clean.txt"]) {
            [carVinNumViewController hideP];
        }
        
        //CAFD
        if ([self.curFileName isEqualToString:@"CAFD_00001EF6_006_020_007.txt"]) {
            [CAFDViewController hideP];
        }
        
        if ([self.curFileName isEqualToString:@"ISTA_contralext.txt"]) {
            
            [self fireAction];
            
//            if (!self.ftimer) {
//
//                NSLog(@"创建");
//                self.ftimer = [NSTimer timerWithTimeInterval:.3 target:self selector:@selector(fireAction) userInfo:nil repeats:YES];
////                [self.ftimer fire];
//                [[NSRunLoop currentRunLoop]addTimer:self.ftimer forMode:NSRunLoopCommonModes];
//
//            }
            
//            [self istaExt:recStr];
            
        }
        
        return ;
        
    }
    
    if ([recStr containsString:@"3e"]) {
        
        //延时组
        if ([self.curFileName isEqualToString:@"I001_send.txt"]||
            [self.curFileName isEqualToString:@"F056_send.txt"]||
            [self.curFileName isEqualToString:@"F025_send.txt"]||
            [self.curFileName isEqualToString:@"F020_send.txt"]||
            [self.curFileName isEqualToString:@"F010_send.txt"]||
            [self.curFileName isEqualToString:@"F001_send.txt"]||
            [self.curFileName isEqualToString:@"VIN.txt"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self sendSheep:self.curFileName];

            });
            
        //不延时发送组
        }else{
            
            [self sendSheep:self.curFileName];
            
        }
        
    }
    
}


-(void)istaExt:(NSString *)recStr{
    
    if ([recStr hasPrefix:@"6"]) {
        
        [ISTAExtViewController receIve:recStr];
        
    }
    
    
}




-(void)fireAction{

    self.sheep= [self.sheepContent count]-1;
    [self sendData:[NSString stringWithFormat:@"%@\r",self.sheepContent[self.sheep]]];

}


//断开连接
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    
    NSLog(@"断开连接");
    self.clientSocket.delegate = nil;
    self.clientSocket = nil;
    self.connected = NO;
    self.firstConnect = YES;
    [self.connectTimer invalidate];
    
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
 
    NSLog(@"服务器IP:%@,端口:%d",host,port);
    // 连接后,可读取服务端的数据
    [self.clientSocket readDataWithTimeout:- 1 tag:0];
    self.connected = YES;
    self.firstConnect = NO;
    self.sheep = 0;
    [self readFile:self.curFileName];
    [self sendSheep:self.curFileName];
    [self showOnProgress:NO];
    
    
}


// 添加定时器
- (void)addTimer
{
    
    if (self.connectTimer) {
        return;
    }
    
    // 长连接定时器
    self.connectTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(longConnectToSocket) userInfo:nil repeats:YES];
    // 把定时器添加到当前运行循环,并且调为通用模式
    [[NSRunLoop currentRunLoop] addTimer:self.connectTimer forMode:NSRunLoopCommonModes];
    
    [self.connectTimer invalidate];
    
    if (!self.connected&&!self.firstConnect) {
        
        [self alertTitle:@"未能安全连接成功" content:@"请确认您的手机已经正确连接适配器的WiFi,无线网络应该为'V-LINK',且无线连接图标也已正常显示,入未能显示请耐心等待手机无线图标正确显示."];
        
        self.firstConnect = YES;
        
    }else{
        
        self.firstConnect = NO;

    }
    
    
}

// 心跳连接
- (void)longConnectToSocket
{
    
    // 发送固定格式的数据,指令@"longConnect"
    float version = [[UIDevice currentDevice] systemVersion].floatValue;
    NSString *longConnect = [NSString stringWithFormat:@"123%f",version];
    NSData  *data = [longConnect dataUsingEncoding:NSUTF8StringEncoding];
    [self.clientSocket writeData:data withTimeout:- 1 tag:0];
    
}
#pragma mark 设置
- (IBAction)settingAction:(id)sender {
    
    
//    carVinNumViewController *car = [[carVinNumViewController alloc]initWithNibName:@"carVinNumViewController" bundle:nil];
//    car.vinNum = @"临00000000000000000";
//    car.carPlat = @"0000000000000000000";
//    car.myblockL = ^(NSInteger m) {
//
//    };
//    car.myblockR = ^(NSInteger m) {
//
//    };
//    [self.navigationController pushViewController:car animated:YES];
    
    settingViewController *setVC = [[settingViewController alloc]initWithNibName:@"settingViewController" bundle:nil];
    UINavigationController *setNC = [[UINavigationController alloc]initWithRootViewController:setVC];
    [self presentViewController:setNC animated:YES completion:nil];
    
    
//
//    recVimViewController *setVC = [[recVimViewController alloc]initWithNibName:@"recVimViewController" bundle:nil];
//    UINavigationController *setNC = [[UINavigationController alloc]initWithRootViewController:setVC];
//     [self presentViewController:setNC animated:YES completion:nil];
    
    
}

-(void)showOnProgress:(BOOL)hiden{
    
    
    self.cplat.hidden = hiden;
    self.carplat.hidden = hiden;
    self.picketView.hidden = hiden;
    self.carForm.hidden = hiden;
    self.platForm.hidden = hiden;
    self.connectBtn.hidden = hiden;
    self.deslabel.hidden = hiden;
    
    self.conningLa.hidden = !hiden;
    self.progreV.hidden = !hiden;
    self.backLine.hidden = !hiden;
    self.frontLine.hidden = !hiden;
    
    if (self.progreV.hidden) {
        [self.progreV stopAnimating];
    }else{
        [self.progreV startAnimating];
    }
    
    self.frontLine.width = 0;
    
    [UIView animateWithDuration:3 animations:^{
        self.frontLine.width = self.backLine.width;

    } completion:^(BOOL finished) {
        
//        carVinNumViewController *car = [[carVinNumViewController alloc]initWithNibName:@"carVinNumViewController" bundle:nil];
//        [self.navigationController pushViewController:car animated:YES];
        
    }];
    
}

- (IBAction)connectionAction:(id)sender {
    
    [self toVinController:@"00000000000000000"];
    
//    return;
    
//
//    NSString * message = @"AT Z\n\r";
//
//    [[bluetoothManager shareInstance] sendMessage:message];
//    
//    return;
    
//    [self showBluetoothList];
    
//    return;
    
//    [self toVinController:@"0000000000000000000000"];
//    return;
    
//    if (self.connected && self.sheep!=self.sheepContent.count) {
//
//        NSLog(@"ww:%ld   %ld",self.sheep,self.sheepContent.count);
//
//        return;
//
//    }
//
    
    if ([[bluetoothManager shareInstance]hasConnected]) {
    
        NSLog(@"mmm");
        self.curFileName = @"VIN.txt";
        self.sheep = 0;
        [self readFile:self.curFileName];
        [self sendSheep:self.curFileName];
        [self showOnProgress:YES];
        
    }else{
        
        [self showBluetoothList];
        
//        [self alertTitle:@"提醒" content:@"请连接"];
        
    }
    
    return;
    
//    NSLog(@"%@",[socketManager getWifiName]);
    
    if (![[socketManager getWifiName]isEqualToString:@"V-LINK"]) {
        [self alertTitle:@"连接失败" content:@"当前软件未能正确连接到您的车辆,请检查您的适配器是否已经正确连接到车辆的OBD接口,且手机通过WiFi或蓝牙连接到OBD适配器上.(WiFi连接选择'V-LINK')"];
        
        return;
        
    }
    
    NSError *error = nil;
    
   BOOL tcon =  [self.clientSocket connectToHost:@"192.168.0.10" onPort:35000 viaInterface:nil withTimeout:30 error:&error];
    self.curFileName = @"VIN.txt";
    
    clientSocket = self.clientSocket;
    
    NSLog(@"%d",tcon);
    
    if (!tcon && [[socketManager getWifiName]isEqualToString:@"V-LINK"]) {
        [self showAlert];
    }
    
}

-(void)showAlert{
    
    [self alertTitle:@"未能安全连接成功" content:@"请确认您的手机已经正确连接适配器的WiFi,无线网络应该为'V-LINK',且无线连接图标也已正常显示,入未能显示请耐心等待手机无线图标正确显示."];
    
}

#pragma mark 显示车架号
-(void)showVinNum:(NSString *)recData{
    
    if (![self.curFileName isEqualToString:@"VIN.txt"]) {
        return;
    }
    
    if (recData.length<15) {
//        NSLog(@"uwh");
        
        return;
    }
    
    if (self.sheep!=20) {
//        NSLog(@"uh");
        return;
    }
    
    [self showOnProgress:NO];
    
    NSLog(@"车架号1:%@",recData);
    
    
    NSString *des1 = recData;
    
    NSLog(@"des1:%@",des1);
    
    des1  = [des1 stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"d车架号2:%@",des1);
    
//    NSArray *Narr = [recData componentsSeparatedByString:@"\n"];
    NSArray *arr = [des1 componentsSeparatedByString:@"\r"];
    
    NSLog(@"==Narr:\n%@",arr);
    
    if (arr.count==1) {
        
        arr = [recData componentsSeparatedByString:@"\n"];
        
    }
    
    NSLog(@"==arr:\n%@",arr);

    if (arr.count<4) {
        return;
    }
    
    NSString *s1 = arr[0];
    NSString *s2 = arr[1];
    NSString *s3 = arr[2];
    NSString *s4 = arr[3];
    
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];
    s1 = [s1 stringByTrimmingCharactersInSet:set];
    s2 = [s2 stringByTrimmingCharactersInSet:set];
    s3 = [s3 stringByTrimmingCharactersInSet:set];
    s4 = [s4 stringByTrimmingCharactersInSet:set];
    
    NSCharacterSet *whitespace = [NSCharacterSet  whitespaceAndNewlineCharacterSet];
    
    s1 = [s1  stringByTrimmingCharactersInSet:whitespace];
    s2 = [s2  stringByTrimmingCharactersInSet:whitespace];
    s3 = [s3  stringByTrimmingCharactersInSet:whitespace];
    s4 = [s4  stringByTrimmingCharactersInSet:whitespace];
    
    NSLog(@"==s1:%@",s1);
    NSLog(@"==s2:%@",s2);
    NSLog(@"==s3:%@",s3);
    NSLog(@"==s4:%@",s4);
    
    
//    NSString *sub1 = [self getDesStr:s1 int:7];
    NSString *sub2 = [self getDesStr:s1 int:15];
    NSString *sub3 = [self getDesStr:s1 int:17];

    NSString *sub4 = [self getDesStr:s2 int:7];
    NSString *sub5 = [self getDesStr:s2 int:9];
    NSString *sub6 = [self getDesStr:s2 int:11];
    NSString *sub7 = [self getDesStr:s2 int:13];
    NSString *sub8 = [self getDesStr:s2 int:15];
    NSString *sub9 = [self getDesStr:s2 int:17];

    NSString *sub10 = [self getDesStr:s3 int:7];
    NSString *sub11 = [self getDesStr:s3 int:9];
    NSString *sub12 = [self getDesStr:s3 int:11];
    NSString *sub13 = [self getDesStr:s3 int:13];
    NSString *sub14 = [self getDesStr:s3 int:15];
    NSString *sub15 = [self getDesStr:s3 int:17];
    
    NSString *sub16 = [self getDesStr:s4 int:7];
    NSString *sub17 = [self getDesStr:s4 int:9];
    NSString *sub18 = [self getDesStr:s4 int:11];
    
    NSString *vim = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",sub2,sub3,sub4,sub5,sub6,sub7,sub8,sub9,sub10,sub11,sub12,sub13,sub14,sub15,sub16,sub17,sub18];
    
    [self toVinController:vim];
    
    NSLog(@"车架号:%@",vim);
    
}


-(void)toVinController:(NSString *)vinnum{
    
    carVinNumViewController *car = [[carVinNumViewController alloc]initWithNibName:@"carVinNumViewController" bundle:nil];
    
    __weak typeof (self) weakself = self;
    
    car.myblockL = ^(NSInteger m) {
        
        if (m == 0) {
            
        }else if (m == 1){
            [self ISTA];
        }else if (m == 2){
            [self cleanAction];
        }
        
    };
    
    car.myblockR = ^(NSInteger m) {
        
        if (m == 0) {
            [weakself F001];
        }else if (m == 1){
//            [self ISTA];
        }else if (m == 2){
            
        }
    };
    
    car.vinNum = vinnum;
    car.carPlatName = self.vimCarPlat;
    car.carPlat = self.vimCarPlat;
    [self.navigationController pushViewController:car animated:YES];
    
    
}


#pragma mark ISTA 功能调用
-(void)ISTA{
    
    self.sheep = 0;
    
    NSString *sendName = [NSString stringWithFormat:@"%@_send.txt",@"ISID"];
    NSString *moduleName = [NSString stringWithFormat:@"%@_module.txt",@"ISID"];
    
    self.asdict = [linkManager readTxtFile:moduleName];
    
    NSLog(@"ISTA_dict:%@",self.asdict);
    
    [ISTAViewController getTitleArr:self.asdict];
    self.curFileName = sendName;
    [self readFile:self.curFileName];
    [self sendSheep:self.curFileName];
    
    
}



#pragma mark 高级刷隐藏发值
-(void)F001{
    
    self.sheep = 0;
    NSString *sendName = [NSString stringWithFormat:@"%@_send.txt",self.vimCarPlat];
    NSString *moduleName = [NSString stringWithFormat:@"%@_module.txt",self.vimCarPlat];
    
    self.asdict = [linkManager readTxtFile:moduleName];
    
    NSLog(@"self.asdict:%@",self.asdict);
    NSLog(@"keys:%@",[self.asdict allKeys]);
    
    [recVimViewController getTitleArr:self.asdict];
    self.curFileName = sendName;
    [self readFile:self.curFileName];
    [self sendSheep:self.curFileName];
    
}

-(void)F001Module:(NSString *)text{
    
    if (![self.curFileName isEqualToString:@"F001_send.txt"]) {
        return;
    }
    
}



-(NSString *)getDesStr:(NSString*)des int:(NSInteger)intm{
    
    return [ViewController stringFromHexString:[des substringWithRange:NSMakeRange(intm, 2)]];
    
}


+ (NSString *)stringFromHexString:(NSString *)hexString { //
    
    char *myBuffer = (char *)malloc((int)[hexString length] / 2 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
    }
    
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    return unicodeString;
    
}

#pragma mark string 转ASCii
-(NSString *)stringToAscII:(NSString *)str{
    
    NSString *string = str;
    int asciiCode = [string characterAtIndex:0];
    return [NSString stringWithFormat:@"%d",asciiCode];

//    return asciiCode;
    
}



#pragma mark 错误提醒
-(void)alertTitle:(NSString *)titleAle content:(NSString *)contentAle{

    UIAlertController *alert  = [UIAlertController alertControllerWithTitle:titleAle message:contentAle preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
