//
//  ISTAExtViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBLabel.h"

@interface ISTAExtViewController : UIViewController
@property (weak, nonatomic) IBOutlet IBLabel *contentLa;
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (nonatomic, strong) NSArray *contentArr;
@property(nonatomic,strong)NSString *vinNum;
@property(nonatomic,strong)NSString *carPlat;
@property(nonatomic,strong)NSString *carPlatName;
+(void)receIve:(NSString*)recStr;
@end
