//
//  ViewController.h
//  BMWLink
//
//  Created by weixuemeng on 2018/3/11.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

typedef void(^doneBlock)(void);
typedef void(^receMesBlock)(NSString *recStr);

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


+(void)sockerFunc:(NSString*)sendMesArr receive:(receMesBlock)receB start:(doneBlock)startBlock end:(doneBlock)endBlock;
+ (NSString *)stringFromHexString:(NSString *)hexString;

@end

