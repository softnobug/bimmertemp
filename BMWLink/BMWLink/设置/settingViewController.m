//
//  settingViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/15.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "settingViewController.h"
#import "unlockTableViewCell.h"
#import "normalTableViewCell.h"
#import "switchTableViewCell.h"
@interface settingViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation settingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"switchTableViewCell" bundle:nil] forCellReuseIdentifier:@"switch"];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"unlockTableViewCell" bundle:nil] forCellReuseIdentifier:@"unlock"];
    [self.tableview registerNib:[UINib nibWithNibName:@"normalTableViewCell" bundle:nil] forCellReuseIdentifier:@"normal"];

    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    self.tableview.backgroundColor = [UIColor colorWithHexString:@"e9eef3"];
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 1) {
        return 2;
    }
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        
        unlockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"unlock"];
        

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    if (indexPath.section == 1&& indexPath.row == 0) {
        
        normalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"normal"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    switchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"switch"];
    cell.swib = ^(BOOL ison) {
        NSLog(@"%d",ison);
    };
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 1) {
        return 40;
    }
    
    return 20;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    
}

-(void)setNav{
    
    self.title = @"设置";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    [self.navigationItem.rightBarButtonItem setTintColor:themeColor];
    
}

-(void)doneAction{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
