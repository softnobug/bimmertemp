//
//  ISTAViewController.m
//  BMWLink
//
//  Created by weixuemeng on 2018/3/25.
//  Copyright © 2018年 com.softnobug. All rights reserved.
//

#import "ISTAViewController.h"

@interface ISTAViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *leftTitle;
@property (weak, nonatomic) IBOutlet UILabel *carVinLa;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *titleLa;
@property (weak, nonatomic) IBOutlet UILabel *leftSubTitle;

@end

@implementation ISTAViewController
//static UILabel *mTitleLa;
//static NSDictionary *mtitDic;

static NSMutableArray *contentArr;
static NSMutableArray *showcontentArr;
static NSMutableArray *showTitContentArr;

static UIView *view;
static UILabel *mTitleLa;

static UITableView *mtableview;
static NSMutableArray *subArr;
static NSDictionary *mtitDic;
static  BOOL hasDone;


- (void)viewDidLoad {
    [super viewDidLoad];
    view = self.view;
    if ([self.titleStr isEqualToString:@"F"]) {
        self.leftTitle.text = @"ISTA+";
        self.leftSubTitle.text = @"功能调用";
    }
    else if ([self.titleStr isEqualToString:@"Y"]){
        self.leftTitle.text = @"ISTA+";
        self.leftSubTitle.text = @"硬件诊断";
    }
    
    NSString *lastVin = [self.vinNum substringWithRange:NSMakeRange(10, 7)];
    self.carVinLa.text = [NSString stringWithFormat:@"当前车辆VIN:%@  车辆平台:%@",lastVin,self.carPlat];
//    mtableview = self.tableview;
    mTitleLa = self.titleLa;
    
    contentArr = @[].mutableCopy;
    showcontentArr = @[].mutableCopy;
    [self initTable];
    
    if ([self.titleStr isEqualToString:@"F"]) {
        
        mTitleLa.text = @"正在读取中...";
        mTitleLa.textColor = [UIColor redColor];
        
    }else{
        mTitleLa.hidden = YES;
        [showcontentArr addObjectsFromArray:[linkManager arrContainArr:@"ISTA_check.txt"]];
        
    }
    
   
    
    
}




+(void)getTitleArr:(NSDictionary*)titDic{
    
//    NSLog(@"isidMtitDict:%@",titDic);
    
    mtitDic = titDic;
        
//    NSLog(@"mtitDic:%@",mtitDic);
    
}

+(void)addDataStr:(NSString *)arrStr{
    
    NSArray *Narr = [arrStr componentsSeparatedByString:@"\n"];
    
    NSMutableArray *arr= @[].mutableCopy;
    
    for (NSString *ts in Narr) {
        
        NSString *tts = [ts  stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        tts = [ts stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        
        if (tts.length==19) {
            
            [arr addObject:tts];
            NSLog(@"rec--");
            
        }else{
            
            NSLog(@"rec--%ld",tts.length);
            
        }
        
    }
    
    NSLog(@"ISTA_ADD_STR:%@",arr);
    
    if (arr.count!=0) {
        
        [contentArr addObject:arr];
        
        [ISTAViewController getTitleName:arr];
        
    }
    
}

+(void)getTitleName:(NSArray *)tArr{
    
    NSString *sub = [tArr[0] substringWithRange:NSMakeRange(0, 3)];
    NSString *titleN = [mtitDic objectForKey:sub];
    
    [showcontentArr addObject:titleN];

    NSMutableArray *subArrT = @[].mutableCopy;
    
    subArrT = [ChineseString LetterSortArray:showcontentArr.mutableCopy];
    
    NSLog(@"subArrT:%@",subArrT);
    
    NSMutableArray *tShowArr = @[].mutableCopy;
    
    for (NSArray *tArr in subArrT) {
        
        for (NSString *ts in tArr) {
            
            [tShowArr addObject:ts];
            
        }
        
    }
    
    NSLog(@"tShowArr:%@",tShowArr);
    
    [showcontentArr removeAllObjects];
    [showcontentArr addObjectsFromArray:tShowArr.mutableCopy];
    NSLog(@"showcontentArr:%@",showcontentArr);
    
    [mtableview reloadData];
    
    
}



//等待中
+(void)showP{
    
    mTitleLa.text = @"正在读取中...";
    mTitleLa.textColor = [UIColor redColor];
    [smartHUD progressShow:view alertText:@"车辆模块读取中,请稍后..."];
    
}

+(void)hideP{
    
    [smartHUD Hide];
    mTitleLa.text = @"读取成功";
    mTitleLa.textColor = [UIColor greenColor];
    hasDone = YES;
    
}



-(void)initTable{
    
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"ISTATableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    self.tableview.estimatedRowHeight = 30;
    
    if (@available(iOS 11.0,*)) {
        
        self.tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        
    }else{
        
        self.automaticallyAdjustsScrollViewInsets = NO;
        
    }
    self.tableview.estimatedSectionHeaderHeight = 0;
    self.tableview.estimatedSectionFooterHeight = 0;
    mtableview = self.tableview;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return showcontentArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    ISTATableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if ([self.titleStr isEqualToString:@"F"]) {
        
        cell.contentLa.text =  showcontentArr[indexPath.row];

    }else{
        
        cell.contentLa.text =  showcontentArr[indexPath.row][0];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.titleStr isEqualToString:@"Y"]) {
        
        ISTAExtViewController *istaEVC = [[ISTAExtViewController alloc]initWithNibName:@"ISTAExtViewController" bundle:nil];
        istaEVC.contentArr = showcontentArr[indexPath.row];
        istaEVC.vinNum = self.vinNum;
        istaEVC.carPlat = self.carPlat;
        [self.navigationController pushViewController:istaEVC animated:YES];
        
    }else{
        
       NSString *CellTitle =  showcontentArr[indexPath.row];

        [mtitDic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            

            if ([obj isEqualToString: CellTitle]) {
                
                NSString *objectEightId = key;
                
                NSLog(@"----------%@",objectEightId);
                
                ISTAFuncViewController *istaEVC = [[ISTAFuncViewController alloc]initWithNibName:@"ISTAFuncViewController" bundle:nil];
                istaEVC.keyStr = objectEightId;
                istaEVC.vinNum = self.vinNum;
                istaEVC.carPlat = self.carPlat;
                [self.navigationController pushViewController:istaEVC animated:YES];
                
                
            }
            
        }];
        
        
        
    }
    
    
    
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)disAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
